package com.dyggyty.espace.graph;

/**
 * Basic object class provides simple linked listing
*/
public class Obj {
    public int Type;

    public Obj Next;
    public Obj Prev;

    public Obj() {
        Type = 0;
        Next = null;
        Prev = null;
    }

    public void linkTo(Obj p) {
        if (this.Next != null || this.Prev != null) this.unlink();
        if (p == null) return;

        this.Next = p.Next;
        this.Prev = p;
        p.Next = this;

        if (this.Next != null)
            this.Next.Prev = this;
    }

    public void unlink() {
        if (this.Prev != null) {
            this.Prev.Next = this.Next;
        }

        if (this.Next != null) {
            this.Next.Prev = this.Prev;
        }

        this.Next = null;
        this.Prev = null;
    }
}

