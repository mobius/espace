package com.dyggyty.espace.graph;

import com.dyggyty.espace.graph.engine.Matrix43;
import com.dyggyty.espace.graph.engine.Model3d;
import com.dyggyty.espace.graph.engine.Vector3D;

/**
 * Simple 3d object class
 */
public class Obj3d extends Obj {
    public Vector3D Position = new Vector3D();
    public Vector3D Angle = new Vector3D();

    public Vector3D renderPos = new Vector3D();
    public Vector3D movement = new Vector3D();

    public Matrix43 Mat = new Matrix43();

    private Vector3D Min = new Vector3D();
    private Vector3D Max = new Vector3D();

    public int Col;
    public int colour;

    public float Size;    // For planets/suns

    private Model3d mod;


    public void dupe(Obj3d o)    // Duplicate basic 3d obj info (not links)
    {
        Position.copy(o.Position);
        Angle.copy(o.Angle);    // Shouldn't be used
        Mat.copy(o.Mat);
        Min.copy(o.Min);        // Just for bounding box display
        Max.copy(o.Max);        // ditto
        Col = o.Col;
        colour = o.colour;
        mod = o.mod;
    }

    public Obj3d() {
        mod = null;
    }

    public void mod(Model3d mod) {
        this.mod = mod;
        if (mod == null)
            return;

        Min.copy(mod.Min);
        Max.copy(mod.Max);
    }

    public Model3d mod() {
        return this.mod;
    }

    public void pos(float x, float y, float z) {
        Position.x = x;
        Position.y = y;
        Position.z = z;
    }

    public void ang(float x, float y, float z) {
        Angle.x = x;
        Angle.y = y;
        Angle.z = z;
    }

    public void randomColour() {
        Col = (int) (Math.random() * 8);
        if (Col < 1) Col = 1;
        if (Col > 7) Col = 7;

        int c = 0;

        if ((Col & 1) != 0) c += 255;
        if ((Col & 2) != 0) c += 255 * 256;
        if ((Col & 4) != 0) c += 255 * 65536;
        colour = c;
    }


    // Collision code follows - should create obj3dcoll class at some stage
// For laser read ray/vector
// For ship read object
    public int collideWithVec(Vector3D pos, Vector3D dir) {
        Vector3D p = new Vector3D(pos),
                d = new Vector3D(dir),
                v = new Vector3D();

        Matrix43 m = new Matrix43();

        float t;

        boolean bHit = false;

        m.affineInverse(this.Mat);
        p.sub(this.Position);

        // make rays source and direction relative to ship
        p.mul(m);
        d.mul(m);

        // any point on ray is p + t*d

        // Do z plane checks
        if (d.z != 0) {
            // find where ray intersects plane z = Min.z i.e. p.z+t*d.z = Min.z, t = (Min.z-p.z)/d.z
            t = (Min.z - p.z) / d.z;
            if (t > 0)    // Laser has specific dir
            {
                v.x = p.x + t * d.x;
                v.y = p.y + t * d.y;

                if (v.x > Min.x && v.x < Max.x && v.y > Min.y && v.y < Max.y)
                    bHit = true;
                else {
                    // z = Max.z
                    t = (Max.z - p.z) / d.z;
                    if (t > 0)    // Laser has specific dir
                    {
                        v.x = p.x + t * d.x;
                        v.y = p.y + t * d.y;
                        if (v.x > Min.x && v.x < Max.x && v.y > Min.y && v.y < Max.y)
                            bHit = true;
                    }
                }
            }
        }

        if (d.x != 0 && !bHit) {
            t = (Min.x - p.x) / d.x;
            if (t > 0)    // Laser has specific dir
            {
                v.y = p.y + t * d.y;
                v.z = p.z + t * d.z;

                if (v.z > Min.z && v.z < Max.z && v.y > Min.y && v.y < Max.y)
                    bHit = true;
                else {
                    // x = Max.x
                    t = (Max.x - p.x) / d.x;
                    if (t > 0)    // Laser has specific dir
                    {
                        v.y = p.y + t * d.y;
                        v.z = p.z + t * d.z;
                        if (v.z > Min.z && v.z < Max.z && v.y > Min.y && v.y < Max.y)
                            bHit = true;
                    }
                }
            }
        }

        if (d.y != 0 && !bHit) {
            // y = Min.y
            t = (Min.y - p.y) / d.y;
            if (t > 0)    // Laser has specific dir
            {
                v.x = p.x + t * d.x;
                v.z = p.z + t * d.z;

                if (v.z > Min.z && v.z < Max.z && v.x > Min.x && v.x < Max.x)
                    bHit = true;
                else {
                    t = (Max.y - p.y) / d.y;
                    if (t > 0)    // Laser has specific dir
                    {
                        v.x = p.x + t * d.x;
                        v.z = p.z + t * d.z;
                        if (v.z > Min.z && v.z < Max.z && v.x > Min.x && v.x < Max.x)
                            bHit = true;
                    }
                }
            }
        }

        // Hit front or rear shields?
        int ColValue = 0;
        if (bHit) {
            if (v.z >= 0) {
                ColValue = 1;
            } else {
                ColValue = 2;
            }
        }

        return (ColValue);
    }
}

