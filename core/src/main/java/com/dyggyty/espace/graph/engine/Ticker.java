package com.dyggyty.espace.graph.engine;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Provide some text display options
 * Just a simple one to start with
 */
public class Ticker {
    private final static int MAX = 20;

    private static boolean Active;
    private static String str[] = new String[MAX];
    private static int Speed,
    TimeCur,
    Current,
    Num;
    private static float SY, SX;

    private static int color;

    public Ticker() {
        Active = false;
    }

    public void setupSimple(String str[], int first, int last, int speed, int sx, float sy,
                            int col) {
        Active = false;

        Num = last - first + 1;

        if (Num < 1 || Num > MAX)
            return;

        for (int i = 0; i != Num; i++) {
            Ticker.str[i] = str[first + i];
        }

        Speed = speed;
        if (Speed < 1)
            return;

        SX = sx;
        SY = sy;
        color = col;

        Current = 0;
        TimeCur = 0;
        Active = true;
    }

    public void update(Canvas g) {
        if (!Active) return;

        if (++TimeCur > Speed) {
            TimeCur = 0;
            if (++Current >= Num)
                Current = 0;
        }

        Paint paint = new Paint();
        paint.setColor(color);
        g.drawText(str[Current], SX, SY, paint);
    }
}