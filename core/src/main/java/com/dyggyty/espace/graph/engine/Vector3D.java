package com.dyggyty.espace.graph.engine;

import com.dyggyty.espace.game.GameCon;

/**
 * 3D vector
 */
public class Vector3D {

    public float x, y, z;
    public int sX, sY;

    public Vector3D() {
        x = 0;
        y = 0;
        z = 0;
    }

    public Vector3D(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3D(Vector3D v) {
        x = v.x;
        y = v.y;
        z = v.z;
    }

    public void zero() {
        x = 0;
        y = 0;
        z = 0;
    }

    public void set(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void copy(Vector3D v) {
        x = v.x;
        y = v.y;
        z = v.z;
    }

    public void normalise() {
        float len = x * x + y * y + z * z;

        len = (float) Math.sqrt((double) len);

        x /= len;
        y /= len;
        z /= len;
    }

    public float size() {
        return ((float) (Math.sqrt(x * x + y * y + z * z)));
    }

    public void add(Vector3D v) {
        this.x += v.x;
        this.y += v.y;
        this.z += v.z;
    }

    public void add(float x, float y, float z) {
        this.x += x;
        this.y += y;
        this.z += z;
    }

    public void sub(Vector3D v) {
        this.x -= v.x;
        this.y -= v.y;
        this.z -= v.z;
    }

    public void sub(Vector3D v, Vector3D v2) {
        this.x = v.x - v2.x;
        this.y = v.y - v2.y;
        this.z = v.z - v2.z;
    }

    public void neg() {
        x = -x;
        y = -y;
        z = -z;
    }

    // Scalar
    public void mul(float s) {
        x *= s;
        y *= s;
        z *= s;
    }

    public void mul(Vector3D v) {
        float x = this.x,
                y = this.y,
                z = this.z;

        this.x = y * v.z - z * v.y;
        this.y = z * v.x - x * v.z;
        this.z = x * v.y - y * v.x;
    }

    public float dot(Vector3D v) {
        float dot;
        dot = x * v.x + y * v.y + z * v.z;
        return (dot);
    }

    public void mul(Matrix43 m) {
        float x = this.x,
                y = this.y,
                z = this.z;

        this.x = x * m.m[0][0] + y * m.m[1][0] + z * m.m[2][0] + m.m[3][0];
        this.y = x * m.m[0][1] + y * m.m[1][1] + z * m.m[2][1] + m.m[3][1];
        this.z = x * m.m[0][2] + y * m.m[1][2] + z * m.m[2][2] + m.m[3][2];
    }

    // Create perspectivised, 2d screen coord, return order table value for point
    public int pers() {
        int i;

        if (z == 0) {
            sX = 0;
            sY = 0;
        } else {
            sX = (int) (x * (GameCon.PERSPECTIVE_H / z)) + (int) Render.getInstance().getScreenCenterX();
            sY = (int) Render.getInstance().getScreenCenterY() - (int) (y * (GameCon.PERSPECTIVE_H / z));
        }

        i = (int) (z / 4);
        if (i < 0) i = 0;
        if (i >= GameCon.NUM_Z_BUCKETS) i = GameCon.NUM_Z_BUCKETS - 1;
        return (i);
    }
}
