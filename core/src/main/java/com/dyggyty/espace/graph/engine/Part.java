package com.dyggyty.espace.graph.engine;

import android.graphics.Color;
import com.dyggyty.espace.game.GameCon;
import com.dyggyty.espace.game.model.Universe;
import com.dyggyty.espace.graph.Obj;

/**
 * Simple particle class
 */
public class Part extends Obj {
    public Vector3D Position = new Vector3D();
    public Vector3D Angle = new Vector3D();

    public Vector3D RenderPos = new Vector3D();

    public Vector3D Movement = new Vector3D();

    public Matrix43 Mat = new Matrix43();

    public int Col;
    public Color Colour;

    public Vector3D Move = new Vector3D();
    public int Life;

    public Part() {
        Life = 0;
        Col = 3;
    }

    public void setup(Matrix43 m, Vector3D Pos) {
        Mat.copy(m);
        Move.set(0f, 10f, -100f);
        Move.mul(Mat);

        Position.copy(Pos);

        Life = 16;
    }

    // Draw object into order table
    public void render(Universe uni, Poly orderTable[], Poly polyFree, Matrix43 m) {
        Matrix43 m2 = new Matrix43();
        Vector3D v = new Vector3D();

        Life--;

        if (Life == 0) {
            linkTo(uni.partFree);
            return;
        }

        Position.add(Move);

        // Can now use m2 for lighting purposes
        m2.copy(m);
        m2.trans(0, 0, 0);

        Poly p = polyFree.next;

        if (p == null)
            return;

        v.copy(Position);
        v.sub(uni.Cam.Position);
        v.mul(m);

        if (v.z < (GameCon.PERSPECTIVE_H / 2)) {
            // -ve z check; i.e. should clip with z=0 (or similar)
            //drawFlag	=	false;
            return;
        }

        if (Render.fovClip(v)) {
            return;
        }

        int iOT = v.pers();

        p.x[0] = v.sX;
        p.y[0] = v.sY;

        p.x[1] = (int) (10 * GameCon.PERSPECTIVE_H / v.z) + 1;
        p.y[1] = p.x[1];

        int colour = 0,
                Intensity = (Life * 16);

        if (Intensity < 0) Intensity = 0;
        if (Intensity > 255) Intensity = 255;

        colour += Intensity * 256;
        colour += (128 + Intensity / 2) * 65536;

        p.color = colour;
        p.iNSides = 1;
        p.linkTo(orderTable[iOT]);
    }
}
