package com.dyggyty.espace.graph.engine;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import com.dyggyty.espace.KeyHandler;
import com.dyggyty.espace.game.GameCon;
import com.dyggyty.espace.game.model.Planet;
import com.dyggyty.espace.game.model.ShipSim;
import com.dyggyty.espace.game.model.Station;
import com.dyggyty.espace.game.model.Universe;
import com.dyggyty.espace.graph.Obj;
import com.dyggyty.espace.graph.Obj3d;

/**
 * Self contained Render class.
 * <p/>
 * This class will handle all screen output and will run on it's own thread.
 */
public class Render {

    private static final Render instance = new Render();

    private static final Paint LAUNCH_PAINT = new Paint();

    static {
        LAUNCH_PAINT.setColor(Color.WHITE);
        LAUNCH_PAINT.setStyle(Paint.Style.STROKE);
    }

    // Static Render value
    public final static float DEFAULT_SCREEN_WIDTH = 640;
    public final static float DEFAULT_SCREEN_HEIGHT = 480;
    private final static int MAX_GAME_SPEED_MS = 40;

    private float screenWidth;
    private float screenHeight;
    private float screenCenterX;
    private float screenCenterY;

    private int fps;
    private int currFps;
    private long fpsTime;
    private long lastScreenDrawTime;

    // List control
    static Obj pobjList;
    static Obj3d objList[] = new Obj3d[2];
    static Obj3d objFree = new Obj3d();
    static Obj3d objects[] = new Obj3d[2 * GameCon.MAX_DYNAMIC_OBJS];

    private Camera cam = new Camera();

    Vector3D Light = new Vector3D(0.7f, 0.7f, 0);

    Poly poly[] = new Poly[GameCon.NUM_Z_BUCKETS];
    Poly polyFree = new Poly();

    Vector3D star[] = new Vector3D[GameCon.NUM_STARS];
    Vector3D star_rel[] = new Vector3D[GameCon.NUM_STARS_REL];

    // Mr Universe & friend
    private Universe universe;
    private ShipSim player;

    // Misc
    Ticker Mess1 = new Ticker();
    int t = 0;

    private Render() {
        setScreenWidth(DEFAULT_SCREEN_WIDTH);
        setScreenHeight(DEFAULT_SCREEN_HEIGHT);
    }

    public Universe getUniverse() {
        return universe;
    }

    public static Render getInstance() {
        return Render.instance;
    }

    public void setScreenWidth(float screenWidth) {
        this.screenWidth = screenWidth;
        screenCenterX = (screenWidth / 2);
    }

    public void setScreenHeight(float screenHeight) {
        this.screenHeight = screenHeight;
        screenCenterY = (screenHeight / 2);
    }

    public void initialise(Universe uni) {
        int i;

        this.universe = uni;

        setScreenHeight(DEFAULT_SCREEN_HEIGHT);
        setScreenWidth(DEFAULT_SCREEN_WIDTH);

        for (i = 0; i != GameCon.NUM_Z_BUCKETS; i++)
            poly[i] = new Poly();

        for (i = 0; i != GameCon.NUM_FREE_POLYS; i++) {
            Poly p = new Poly();
            p.linkTo(polyFree);
        }

        for (i = 0; i != GameCon.NUM_STARS; i++) {
            star[i] = new Vector3D((float) ((Math.random() - 0.5) * 10000),
                    (float) ((Math.random() - 0.5) * 10000),
                    (float) ((Math.random() - 0.5) * 10000));
        }

        for (i = 0; i != GameCon.NUM_STARS_REL; i++) {
            star_rel[i] = new Vector3D((float) (Math.random() * 32000) - 16000,
                    (float) (Math.random() * 32000) - 16000,
                    (float) (Math.random() * 32000) - 16000);
        }

        for (i = 0; i != 2; i++) {
            objList[i] = new Obj3d();
        }

        for (i = 0; i != 2 * GameCon.MAX_DYNAMIC_OBJS; i++) {
            objects[i] = new Obj3d();
            objects[i].linkTo(objFree);
        }

        Mess1.setupSimple(GameCon.MISC, 27, 33, 100, 500, screenHeight - 10, Color.WHITE);
    }

    public void rePaint(Canvas canvas) {
        if (universe.Cam == null) {
            return;
        }

        this.cam.copy(universe.Cam);

        Render.pobjList = universe.ShipUsed;
        paintWorld(canvas);

        addToFps();
    }

    public void setupList(ShipSim player) {
        this.player = player;
    }

    public void paintWorld(Canvas canvas) {
        t++;

        Paint rectPaint = new Paint();
        rectPaint.setColor(Color.BLACK);
        canvas.drawRect(0f, 0f, screenWidth, screenHeight, rectPaint);

        doMain(canvas, player);
    }


    public void doMain(Canvas canvas, ShipSim s) {
        switch (GameCon.CURRENT_MODE) {
            case GameCon.MODE_TITLE:
                if (GameCon.TIME_IN_MODE != 0) {
                    drawStars(canvas);

                    if (pobjList != null) {
                        drawShips(canvas, (Obj3d) pobjList.Next);
                        Poly.drawOrderTable(canvas, poly, polyFree);
                    }
                }
                break;

            case GameCon.MODE_DOCKED:
                if (s == null)
                    return;

                switch (s.iView) {
                    case 4:
                        drawEquip(s);
                        break;
                    case 5:
                        drawGalaxy(s);
                        break;
                    case 6:
                        drawLocal(s);
                        break;
                    case 7:
                        drawPlanetData(s);
                        break;
                    case 8:
                        drawMarketPrices(s, universe.station);
                        break;    // Naughty
                    case 9:
                        drawShipStatus(canvas, s);
                        break;
                    case 0:
                        drawShipCargo(s);
                        break;
                }

                Paint rectPaint = new Paint();
                rectPaint.setColor(Color.WHITE);

                if (s.iView == 4) {
                    Misc.drawStringCentre(canvas, rectPaint, GameCon.VIEW[10], 20);
                } else {
                    Misc.drawStringCentre(canvas, rectPaint, GameCon.VIEW[s.iView], 20);
                }

                Mess1.update(canvas);
                break;

            case GameCon.MODE_SPACE:
                if (s.iView > 0 && s.iView < 5) {
                    drawStars(canvas);

                    drawBodies(canvas, universe.Bodies);

                    drawPart();

                    if (pobjList != null) {
                        drawShips(canvas, (Obj3d) pobjList.Next);
                    }

                    Poly.drawOrderTable(canvas, poly, polyFree);

                    if (s.bShoot) {
                        drawLasers(s);
                    }

                    drawHUD(canvas, s);
                } else {
                    rectPaint = new Paint();
                    rectPaint.setColor(Color.WHITE);

                    canvas.drawRect(32f, 32f, screenWidth - 64, screenHeight - 64, rectPaint);

                    switch (s.iView) {
                        case 5:
                            drawGalaxy(s);
                            break;
                        case 6:
                            drawLocal(s);
                            break;
                        case 7:
                            drawPlanetData(s);
                            break;
                        case 8:
                            drawMarketPrices(s, universe.station);
                            break;    // Naughty
                        case 9:
                            drawShipStatus(canvas, s);
                            break;
                        case 0:
                            drawShipCargo(s);
                            break;
                    }
                }

                rectPaint = new Paint();
                rectPaint.setColor(Color.WHITE);

                KeyHandler keyHandler = KeyHandler.getInstance();
                Misc.drawStringCentre(canvas, rectPaint, GameCon.VIEW[s.iView] +
                        " FPS: " + fps +
                        "; Azimuth: " + String.valueOf(keyHandler.getAzimuth()) +
                        "; Pitch: " + String.valueOf(keyHandler.getPitch()) +
                        ": Roll: " + String.valueOf(keyHandler.getRoll()), 20);
                break;

            case GameCon.MODE_DOCK:
            case GameCon.MODE_LAUNCH:
                int t = GameCon.TIME_IN_MODE % 200;
                int t2 = t % 25;

                t2 *= 8;
                for (int i = t2 - 4; i < t2 + 4; i++) {
                    if (i >= 0) {
                        canvas.drawCircle(screenCenterX, screenCenterY, i * 4, LAUNCH_PAINT);
                    }
                }
                break;

            case GameCon.MODE_HYPER:
                s.iView = 1;

                drawStars(canvas);
                drawBodies(canvas, universe.Bodies);

                if (pobjList != null) {
                    drawShips(canvas, (Obj3d) pobjList.Next);
                    Poly.drawOrderTable(canvas, poly, polyFree);
                }

                t = GameCon.TIME_IN_MODE % 200;
                t2 = t % 25;

                rectPaint = new Paint();
                rectPaint.setColor(Color.WHITE);

                t2 *= 8;
                for (int i = t2 - 4; i < t2 + 4; i++) {
                    if (i >= 0) {
                        RectF oval = new RectF(screenCenterX - (i * 4), screenCenterY - (i * 4), i * 8, i * 8);
                        canvas.drawOval(oval, rectPaint);
                    }
                }

                drawHUD(canvas, s);
                break;

            case GameCon.MODE_AUTODOCK1:
                break;

            case GameCon.MODE_AUTODOCK2:
                break;
        }
    }


    private void drawStars(Canvas canvas) {
        Vector3D v = new Vector3D();

        Paint rectPaint = new Paint();
        rectPaint.setColor(Color.WHITE);

        for (int i = 0; i != GameCon.NUM_STARS; i++) {
            v.copy(star[i]);
            v.mul(cam.Mat);
            v.pers();

            canvas.drawRect(v.sX, v.sY, v.sX + 1, v.sY + 1, rectPaint);
        }

        // Stars relative to cameras motion
        for (int i = 0; i != GameCon.NUM_STARS_REL; i++) {
            int x, y, z;

            v.copy(star_rel[i]);

            v.sub(cam.Position);

            x = (int) v.x;
            y = (int) v.y;
            z = (int) v.z;

            if (x >= 0) {
                x = x & 0x7fff;
            } else {
                x = 0x7fff - ((-x) & 0x7fff);
            }

            if (y >= 0) {
                y = y & 0x7fff;
            } else {
                y = 0x7fff - ((-y) & 0x7fff);
            }

            if (z >= 0) {
                z = z & 0x7fff;
            } else {
                z = 0x7fff - ((-z) & 0x7fff);
            }

            x -= 0x4000;
            y -= 0x4000;
            z -= 0x4000;

            v.x = x;
            v.y = y;
            v.z = z;

            v.mul(cam.Mat);

            if (v.z >= GameCon.PERSPECTIVE_H / 2) {
                v.pers();
                if (v.z > 5000) {
                    canvas.drawRect(v.sX, v.sY, v.sX + 1, v.sY + 1, rectPaint);
                } else {
                    canvas.drawRect(v.sX, v.sY, v.sX + 2, v.sY + 2, rectPaint);
                }
            }
        }
    }

    private void drawPart() {
        Part p = (Part) universe.PartUsed.Next,
                pNext;    // Particles may kill themselves

        while (p != null) {
            pNext = (Part) p.Next;
            p.render(universe, poly, polyFree, cam.Mat);
            p = pNext;
        }
    }

    private void drawBodies(Canvas canvas, Obj Bodies) {
        Obj p = Bodies.Next;

        while (p != null) {
            switch (p.Type) {
                case Universe.OBJ_STATION:
                    drawStation(canvas, (Station) p);
                    break;

                case Universe.OBJ_SUN:
                case Universe.OBJ_PLANET:
                    Poly poly = polyFree.next;

                    if (poly != null) {
                        Vector3D v = new Vector3D(((Obj3d) p).Position);
                        v.sub(cam.Position);
                        v.mul(cam.Mat);
                        if (v.z > GameCon.PERSPECTIVE_H / 2) {
                            int Size = (int) (((Obj3d) p).Size * GameCon.PERSPECTIVE_H / v.z) / 2;
                            int iOT = v.pers();

                            if (Size > 1000) {
                                Size = 1000;
                            }

                            poly.x[0] = v.sX;
                            poly.y[0] = v.sY;

                            poly.x[1] = poly.y[1] = Size;

                            poly.color = ((Obj3d) p).colour;
                            poly.iNSides = 1;
                            poly.linkTo(this.poly[iOT]);
                        }

                    }
                    break;
            }

            p = p.Next;
        }
    }

    private void drawStation(Canvas canvas, Station s) {
        Matrix43 m = new Matrix43();

        s.run();

        m.copy(s.Mat);
        m.trans(s.Position.x - cam.Position.x,
                s.Position.y - cam.Position.y,
                s.Position.z - cam.Position.z);

        m.mul(cam.Mat);

        // Calculate pos relative to cam for radar AND occlusion purposes
        s.renderPos.set(0f, 0f, 0f);
        s.renderPos.mul(m);

        boolean occlude = fovClip(s.renderPos);

        if (!occlude) {
            Model3d mod = s.mod();

            mod.render(poly, polyFree, Light, m);

            if (GameCon.bBounding) {
                mod.renderBounds(canvas, m);
            }
        }
    }

    private void drawShips(Canvas canvas, Obj3d s) {
        Matrix43 m = new Matrix43();
        int iCount = 0;

        Paint rectPaint = new Paint();
        rectPaint.setColor(Color.WHITE);

        while (s != null) {
            if (GameCon.bDebug) {
                canvas.drawText("Ship " + iCount + ":" + s + ":@ " + s.Position.x + "," +
                        s.Position.y +
                        "," +
                        s.Position.z,
                        10, iCount * 15 + 120, rectPaint);
                iCount++;
            }

            m.copy(s.Mat);
            m.trans(s.Position.x - cam.Position.x,
                    s.Position.y - cam.Position.y,
                    s.Position.z - cam.Position.z);

            m.mul(cam.Mat);

            // Calculate pos relative to cam for radar AND occlusion purposes
            s.renderPos.set(0f, 0f, 0f);
            s.renderPos.mul(m);


            boolean occlude;

            if (s == universe.ShipPlayer[0]) {
                occlude = true;
            } else {
                occlude = fovClip(s.renderPos);
            }

            if (!occlude) {
                Model3d mod = s.mod();

                s.renderPos.pers();

                if (s.renderPos.z > 20000) {
                    // So far away, just draw a dot

                    Paint paint = new Paint();
                    paint.setColor(s.colour);
                    paint.setStyle(Paint.Style.FILL);
                    canvas.drawRect(s.renderPos.sX, s.renderPos.sY, s.renderPos.sX + 2, s.renderPos.sY + 2, paint);
                } else {
                    mod.render(poly, polyFree, Light, m);

                    if (GameCon.bBounding) {
                        mod.renderBounds(canvas, m);
                    }
                }

                ShipSim s2 = (ShipSim) s;
                if (!s2.PlayerCraft && s2.bShoot && s2.sBest != null) {
/*
                    graphics.setColor(s.colour);
                    graphics.drawLine(s.renderPos.sX, s.renderPos.sY,
                            screenCenterX + (int) (screenCenterX * Math.random()),
                            screenCenterY + (int) (screenCenterY * Math.random()));
*/
                }
            }
            s = (Obj3d) s.Next;
        }
    }

    private void drawGalaxy(ShipSim s) {
        int BestPlanet = -1,
                BestDistance = 1000000,
                d;

        // Fuel range
        int rad = (int) ((s.Fuel / 7.00) * 20);
        /*graphics.setColor(Color.lightGray);
        drawO((int) s.currentPlanet.sPos.x, (int) s.currentPlanet.sPos.z, rad * 2, rad * 2);

        graphics.setColor(Color.yellow);
        graphics.drawLine((int) s.currentPlanet.sPos.x, (int) s.currentPlanet.sPos.z - 10,
                (int) s.currentPlanet.sPos.x, (int) s.currentPlanet.sPos.z + 10);
        graphics.drawLine((int) s.currentPlanet.sPos.x - 10, (int) s.currentPlanet.sPos.z,
                (int) s.currentPlanet.sPos.x + 10, (int) s.currentPlanet.sPos.z);

        graphics.setColor(Color.red);
        graphics.drawLine((int) s.selectedPlanet.sPos.x, (int) s.selectedPlanet.sPos.z - 10,
                (int) s.selectedPlanet.sPos.x, (int) s.selectedPlanet.sPos.z + 10);
        graphics.drawLine((int) s.selectedPlanet.sPos.x - 10, (int) s.selectedPlanet.sPos.z,
                (int) s.selectedPlanet.sPos.x + 10, (int) s.selectedPlanet.sPos.z);

        graphics.setColor(Color.white);
        for (int i = 0; i != GameCon.NUM_PLANETS; i++) {
            graphics.fillRect((int) universe.planets[i].sPos.x, (int) universe.planets[i].sPos.z, 2, 2);

            d = Misc.SQR((int) universe.planets[i].sPos.x - universe.MouseX) +
                    Misc.SQR((int) universe.planets[i].sPos.z - universe.MouseY);
            if (d < BestDistance) {
                BestPlanet = i;
                BestDistance = d;
            }

        }

        if (universe.MouseClick && BestPlanet != -1) {
            s.iSelectedPlanet = BestPlanet;
            s.selectedPlanet = universe.planets[BestPlanet];
        }

        graphics.setColor(Color.red);
        graphics.drawOval((int) universe.planets[BestPlanet].sPos.x - 7,
                (int) universe.planets[BestPlanet].sPos.z - 7, 14, 14);

        graphics.setColor(Color.white);
        if (s.iSelectedPlanet != s.iCurrentPlanet) {
            graphics.drawString("Distance:" + s.selectedPlanet.distanceFrom(s.currentPlanet), 32,
                    20);
        }
        graphics.drawString(GameCon.MISC[6] + " " + s.currentPlanet.Name, 32,
                GameCon.SCREEN_HEIGHT - 10);
        Misc.drawStringCentre(graphics, GameCon.MISC[7] + " " + s.selectedPlanet.Name,
                GameCon.SCREEN_HEIGHT - 10); */
    }

    private void drawLocal(ShipSim s) {
        int BestPlanet = -1,
                BestDistance = 1000000,
                d,
                cx = ((int) s.currentPlanet.sPos.x),
                cy = ((int) s.currentPlanet.sPos.z),
                x,
                y;

        // Fuel range
        int rad = (int) ((s.Fuel / 7.00) * 20);
        /*graphics.setColor(Color.lightGray);
        drawO(GameCon.SCREEN_CEN_X, GameCon.SCREEN_CEN_Y, rad * 20, rad * 20);

        graphics.setColor(Color.yellow);
        graphics.drawLine(GameCon.SCREEN_CEN_X, GameCon.SCREEN_CEN_Y - 10, GameCon.SCREEN_CEN_X,
                GameCon.SCREEN_CEN_Y + 10);
        graphics.drawLine(GameCon.SCREEN_CEN_X - 10, GameCon.SCREEN_CEN_Y, GameCon.SCREEN_CEN_X +
                10,
                GameCon.SCREEN_CEN_Y);

        x = (((int) s.selectedPlanet.sPos.x) - cx) * 10;
        y = (((int) s.selectedPlanet.sPos.z) - cy) * 10;
        if (Math.abs(x) < (GameCon.SCREEN_WIDTH - 64) / 2 && Math.abs(y) <
                (GameCon.SCREEN_HEIGHT - 64) / 2) {
            graphics.setColor(Color.red);

            x += GameCon.SCREEN_CEN_X;
            y += GameCon.SCREEN_CEN_Y;

            graphics.drawLine(x, y - 10, x, y + 10);
            graphics.drawLine(x - 10, y, x + 10, y);
        }


        graphics.setColor(Color.white);
        for (int i = 0; i != GameCon.NUM_PLANETS; i++) {
            x = (((int) universe.planets[i].sPos.x) - cx) * 10;
            y = (((int) universe.planets[i].sPos.z) - cy) * 10;

            if (Math.abs(x) < (GameCon.SCREEN_WIDTH - 64) / 2 &&
                    Math.abs(y) < (GameCon.SCREEN_HEIGHT - 64) / 2) {
                x += GameCon.SCREEN_CEN_X;
                y += GameCon.SCREEN_CEN_Y;

                graphics.fillRect(x, y, 2, 2);

                d = Misc.SQR(x - universe.MouseX) + Misc.SQR(y - universe.MouseY);
                if (d < BestDistance) {
                    BestPlanet = i;
                    BestDistance = d;
                }
            }
        }

        if (universe.MouseClick && BestPlanet != -1) {
            s.iSelectedPlanet = BestPlanet;
            s.selectedPlanet = universe.planets[BestPlanet];
        }

        x = (((int) universe.planets[BestPlanet].sPos.x) - cx) * 10;
        y = (((int) universe.planets[BestPlanet].sPos.z) - cy) * 10;
        if (Math.abs(x) < (GameCon.SCREEN_WIDTH - 64) / 2 && Math.abs(y) <
                (GameCon.SCREEN_HEIGHT - 64) / 2) {
            graphics.setColor(Color.red);

            x += GameCon.SCREEN_CEN_X;
            y += GameCon.SCREEN_CEN_Y;

            graphics.drawOval(x - 7, y - 7, 14, 14);
        }

        graphics.setColor(Color.white);
        if (s.iSelectedPlanet != s.iCurrentPlanet) {
            graphics.drawString("Distance:" + s.selectedPlanet.distanceFrom(s.currentPlanet), 32,
                    20);
        }
        graphics.drawString(GameCon.MISC[6] + " " + s.currentPlanet.Name, 32,
                GameCon.SCREEN_HEIGHT - 10);
        Misc.drawStringCentre(graphics, GameCon.MISC[7] + " " + s.selectedPlanet.Name,
                GameCon.SCREEN_HEIGHT - 10);   */
    }

    private void drawPlanetData(ShipSim s) {
        int y;
        Planet p = s.selectedPlanet;

        /*graphics.setColor(Color.white);
        graphics.drawString(
                GameCon.MISC[9] + " " + p.Name + "  (" + GameCon.COLOURS[p.iPlanetC] + ")", 100,
                60);

        graphics.setColor(Color.lightGray);
        y = 92;
        graphics.drawString(GameCon.MISC[8], 100, y);
        graphics.drawString(GameCon.INDUSTRY[p.iIndustry] + "  (" + GameCon.MISC[0] + p.iTechLevel +
                ")",
                200, y);
        y += 16;
        graphics.drawString(GameCon.MISC[3], 100, y);
        graphics.drawString(GameCon.POLITICS[p.iPolitics], 200, y);
        y += 16;
        graphics.drawString(GameCon.MISC[4], 100, y);
        graphics.drawString(GameCon.COLOURS[p.iSpeciesC] + " " + GameCon.SPECIES[p.iSpecies], 200,
                y);
        y += 16;

        y += 32;
        graphics.drawString(GameCon.MISC[2], 100, y);
        y += 16;
        graphics.drawString(GameCon.MISC[1], 120, y);
        y += 16;*/
    }

    private void drawMarketPrices(ShipSim s, Station station) {
        boolean bDocked = (GameCon.CURRENT_MODE == GameCon.MODE_DOCKED);

        int Sel = -1;

        /* graphics.setColor(Color.white);
      graphics.drawString(GameCon.MISC[13], 100, 60);
      graphics.drawString(GameCon.MISC[14], 150, 60);
      graphics.drawString(GameCon.MISC[15], 400, 60);

      if (bDocked)
          graphics.drawString(GameCon.MISC[17], 500, 60);

      int y = 80;
      for (int i = 0; i != GameCon.NUM_PRODUCTS; i++) {
          if (bDocked) {
              if (station.numAvailable[i] == 0)
                  graphics.setColor(Color.darkGray);
              else if (universe.MouseY >= y - 10 && universe.MouseY <= y) {
                  graphics.setColor(Color.red);
              } else
                  graphics.setColor(Color.lightGray);
          } else
              graphics.setColor(Color.lightGray);

          if (universe.MouseY >= y - 10 && universe.MouseY <= y)
              Sel = i;

          graphics.drawString("" + station.numAvailable[i], 100, y);
          graphics.drawString(GameCon.ITEM[i], 150, y);
          graphics.drawString("" + ((float) station.price[i]) / 10, 400, y);

          if (bDocked) {
              if (s.Cargo[i] == 0)
                  graphics.setColor(Color.darkGray);
              else if (universe.MouseY >= y - 10 && universe.MouseY <= y)
                  graphics.setColor(Color.red);
              else
                  graphics.setColor(Color.lightGray);

              graphics.drawString("" + s.Cargo[i], 500, y);
          }

          y += 16;
      }

      if (bDocked) {
          y += 32;
          graphics.setColor(Color.white);
          graphics.drawString(GameCon.MISC[16] + ": " + ((float) s.Credits) / 10, 100, y);

          y += 64;

          Misc.drawStringCentre(graphics, GameCon.MISC[18], GameCon.SCREEN_HEIGHT - 10);

          if (universe.MouseClick) {
              universe.MouseClick = false;
              if (Sel >= 0) {
                  if (station.numAvailable[Sel] > 0 && station.price[Sel] <= s.Credits) {
                      station.numAvailable[Sel]--;
                      s.Cargo[Sel]++;
                      s.Credits -= station.price[Sel];
                  }
              }
          }
      }  */
    }

    /**
     * @deprecated replaced by DockActivity
     */
    private void drawShipStatus(Canvas canvas, ShipSim s) {
    }

    private void drawShipCargo(ShipSim s) {
        boolean flag = false;

        int y = 80;
        /*for (int i = 0; i != GameCon.NUM_PRODUCTS; i++) {
            if (s.Cargo[i] != 0) {
                graphics.setColor(Color.lightGray);

                graphics.drawString("" + s.Cargo[i], 100, y);
                graphics.drawString(GameCon.ITEM[i], 150, y);

                flag = true;

                y += 16;
            }
        }

        graphics.setColor(Color.white);
        if (flag) {
            graphics.drawString(GameCon.MISC[13], 100, 60);
            graphics.drawString(GameCon.MISC[14], 150, 60);
        } else {
            Misc.drawStringCentre(graphics, GameCon.MISC[19], 60);
        }  */
    }

    private void drawEquip(ShipSim s) {
        int y,
                Item = -1,
                Tech = s.currentPlanet.iTechLevel;

        /*graphics.setColor(Color.white);
        graphics.drawString(GameCon.MISC[14], 100, 60);
        graphics.drawString(GameCon.MISC[15], GameCon.SCREEN_CEN_X, 60);

        y = 80;
        for (int i = 4; i != 14; i++) {
            if (GameCon.UPTECH[i] <= Tech) {
                if (universe.MouseY >= y - 10 && universe.MouseY <= y) {
                    graphics.setColor(Color.red);
                    Item = i;
                } else
                    graphics.setColor(Color.lightGray);

                graphics.drawString(GameCon.UPGRADES[i], 100, y);
                graphics.drawString("" + ((float) GameCon.UPCOST[i]) / 10, GameCon.SCREEN_CEN_X, y);
                y += 16;
            }
        }

        for (int i = 0; i != 4; i++) {
            if (GameCon.UPTECH[i] <= Tech) {
                if (universe.MouseY >= y - 10 && universe.MouseY <= y) {
                    graphics.setColor(Color.red);
                    Item = i;
                } else
                    graphics.setColor(Color.lightGray);

                graphics.drawString(GameCon.UPGRADES[i], 100, y);
                graphics.drawString("" + ((float) GameCon.UPCOST[i]) / 10, GameCon.SCREEN_CEN_X, y);
                y += 16;
            }
        }

        y += 32;
        graphics.setColor(Color.white);
        graphics.drawString(GameCon.MISC[16] + ": " + ((float) s.Credits) / 10, 100, y);
        y += 20;

        if (universe.MouseClick) {
            switch (universe.EquipState) {
                case 0:
                    if (Item != -1) {
                        universe.EquipItem = Item;
                        universe.EquipState = 1;
                        universe.MouseClick = false;
                    }
                    break;

                case 1:
                    if (Item != -1) {
                        universe.EquipItem = Item;
                        universe.MouseClick = false;
                    }
                    break;
            }
        }

        switch (universe.EquipState) {
            case 0:
                break;

            case 1:
                graphics.setColor(Color.white);

                if (universe.EquipItem > 3) {
                    if (universe.MouseY >= y - 10 && universe.MouseY <= y) {
                        graphics.setColor(Color.red);
                        if (universe.MouseClick) {
                            universe.MouseClick = false;
                            if (s.Credits >= GameCon.UPCOST[universe.EquipItem]) {
                                switch (universe.EquipItem) {
                                    case 4:    //	Fuel
                                        if (s.Fuel < 7) {
                                            s.Credits -= GameCon.UPCOST[universe.EquipItem];
                                            s.Fuel = 7;
                                        }
                                        break;

                                    case 5:    // Missile
                                        if (s.NumMissiles < 4) {
                                            s.Credits -= GameCon.UPCOST[universe.EquipItem];
                                            s.NumMissiles++;
                                        }
                                        break;

                                    default:
                                        if (universe.EquipItem > 5) {
                                            if (s.Upgrade[universe.EquipItem - 6] == false) {
                                                s.Credits -= GameCon.UPCOST[universe.EquipItem];
                                                s.Upgrade[universe.EquipItem - 6] = true;
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                    } else
                        graphics.setColor(Color.white);

                    graphics.drawString("Buy: " + GameCon.UPGRADES[universe.EquipItem], 100, y);
                    y += 16;
                } else {
                    // Do laser purchase
                    graphics.setColor(Color.white);
                    graphics.drawString("Buy: " + GameCon.UPGRADES[universe.EquipItem], 100, y);
                    y += 16;

                    for (int i = 0; i != 4; i++) {
                        if (universe.MouseY >= y - 10 && universe.MouseY <= y) {
                            graphics.setColor(Color.red);
                            if (universe.MouseClick) {
                                universe.MouseClick = false;

                                if (s.LaserType[i] == -1) {
                                    // New laser for port
                                    if (s.Credits >= GameCon.UPCOST[universe.EquipItem]) {
                                        s.Credits -= GameCon.UPCOST[universe.EquipItem];
                                        s.LaserType[i] = universe.EquipItem;
                                    }
                                } else if (s.LaserType[i] != universe.EquipItem) {
                                    // Upgrade/downgrade
                                    int Cred = s.Credits;
                                    Cred += GameCon.UPCOST[s.LaserType[i]] / 2;

                                    if (Cred >= GameCon.UPCOST[universe.EquipItem]) {
                                        s.Credits = Cred - GameCon.UPCOST[universe.EquipItem];
                                        s.LaserType[i] = universe.EquipItem;
                                    }
                                }
                            }
                        } else
                            graphics.setColor(Color.white);

                        graphics.drawString("--->" + GameCon.MISC[20 + i], 100, y);
                        y += 16;
                    }
                }

                break;
        }    */
    }

    private void drawLasers(ShipSim s) {
        /*int iTX = SCREEN_CEN_X + (int) ((Math.random() - 0.5) * 6),
                iTY = SCREEN_CEN_Y + (int) ((Math.random() - 0.5) * 6);

        int Type = s.LaserType[s.iView - 1];

        switch (Type) {
            case 0:
                graphics.setColor(Color.red);
                break;
            case 1:
                graphics.setColor(Color.green);
                break;
            case 2:
                graphics.setColor(Color.yellow);
                break;
            case 3:
                graphics.setColor(Color.blue);
                break;
        }


        graphics.drawLine(0, SCREEN_HEIGHT - 8, iTX, iTY);
        graphics.drawLine(8, SCREEN_HEIGHT, iTX, iTY);


        graphics.drawLine(SCREEN_WIDTH, SCREEN_HEIGHT - 8, iTX, iTY);
        graphics.drawLine(SCREEN_WIDTH - 8, SCREEN_HEIGHT, iTX, iTY);

        if (Type == 3) {
            graphics.drawLine(0, SCREEN_CEN_Y - 8, iTX, iTY);
            graphics.drawLine(0, SCREEN_CEN_Y - 16, iTX, iTY);

            graphics.drawLine(SCREEN_WIDTH, SCREEN_CEN_Y - 8, iTX, iTY);
            graphics.drawLine(SCREEN_WIDTH, SCREEN_CEN_Y - 16, iTX, iTY);
        }       */
    }

    private void drawHUD(Canvas canvas, ShipSim s) {

        Vector3D v = new Vector3D(s.currentPlanet.Position);
        v.sub(cam.Position);

        float x = 8;
        float y = screenHeight - 106;

        drawStatBox(canvas, x, y, s.FrontShields, s.FrontShieldsMax);
        drawStatBox(canvas, x, y + 16, s.RearShields, s.RearShieldsMax);
        drawStatBox(canvas, x, y + 32, s.Fuel, 7);
        drawStatBox(canvas, x, y + 48, s.CabinTemp, 100);
        drawStatBox(canvas, x, y + 64, s.LaserTemp, 100);
        drawStatBox(canvas, x, y + 80, v.size(), 10000);
        drawStatBox(canvas, x, y + 96, 0, 1);

        if (s.NumMissiles != 0) {
            Paint missilePaint = new Paint();
            missilePaint.setColor(Color.YELLOW);
            for (int i = 0; i < s.NumMissiles; i++) {
                if (i == (s.NumMissiles - 1)) {
                    switch (s.MissileState) {
                        case 1:
                            missilePaint.setColor(Color.RED);
                            break;
                        case 2:
                            missilePaint.setColor(Color.GREEN);
                            break;
                    }
                }
                canvas.drawText("#", x + i * 16, y + 106 - 1, missilePaint);
            }
        }

        float x2 = x + 70;

        Paint textPaint = new Paint();
        textPaint.setColor(Color.YELLOW);
        canvas.drawText("F.Shield", x2, y + 10, textPaint);
        canvas.drawText("R.Shield", x2, y + 26, textPaint);
        canvas.drawText("Fuel", x2, y + 42, textPaint);
        canvas.drawText("C.Temp", x2, y + 58, textPaint);
        canvas.drawText("L.Temp", x2, y + 74, textPaint);
        canvas.drawText("Altitude", x2, y + 90, textPaint);

        /*
        switch (s.MissileState) {
            case 0:
                break;
            case 1:
                graphics.setColor(Color.red);
                graphics.drawString("Targeting", x2, y + 5 + 96);
                break;
            case 2:
                graphics.setColor(Color.green);
                graphics.drawString("Locked", x2, y + 5 + 96);
                break;
        }*/

        x = screenWidth - 70;
        y = screenHeight - 106;
        drawStatBox(canvas, x, y, s.fSpeedTar, s.fSpeedMax);
        drawStatBox(canvas, x, y + 16, s.Rcur.z, s.Rtar.z, s.Rmax.z);
        drawStatBox(canvas, x, y + 32, -s.Rcur.x, -s.Rtar.x, s.Rmax.x);
        drawStatBox(canvas, x, y + 48, s.EnergyBank[3], s.EnergyBankMax);
        drawStatBox(canvas, x, y + 64, s.EnergyBank[2], s.EnergyBankMax);
        drawStatBox(canvas, x, y + 80, s.EnergyBank[1], s.EnergyBankMax);
        drawStatBox(canvas, x, y + 96, s.EnergyBank[0], s.EnergyBankMax);

        x2 = (int) x - 8;

        Misc.drawStringRight(canvas, textPaint, "Speed", x2, (int) y + 10);
        Misc.drawStringRight(canvas, textPaint, "Pitch", x2, (int) y + 26);
        Misc.drawStringRight(canvas, textPaint, "Roll", x2, (int) y + 42);
        Misc.drawStringRight(canvas, textPaint, "Bank3", x2, (int) y + 58);
        Misc.drawStringRight(canvas, textPaint, "Bank2", x2, (int) y + 74);
        Misc.drawStringRight(canvas, textPaint, "Bank1", x2, (int) y + 80);
        Misc.drawStringRight(canvas, textPaint, "Bank0", x2, (int) y + 106);

        //drawRadar(s, screenWidth / 2, screenHeight - 52);
        /*
        y = SCREEN_HEIGHT - 120;
        graphics.setColor(Color.yellow);
        graphics.drawLine(0, y, SCREEN_WIDTH / 5, y);
        graphics.drawLine(SCREEN_WIDTH / 5, y, SCREEN_WIDTH / 5 + 16, y + 16);
        graphics.drawLine(SCREEN_WIDTH / 5 + 16, y + 16, SCREEN_WIDTH * 4 / 5 - 16, y + 16);
        graphics.drawLine(SCREEN_WIDTH * 4 / 5 - 16, y + 16, SCREEN_WIDTH * 4 / 5, y);
        graphics.drawLine(SCREEN_WIDTH * 4 / 5, y, SCREEN_WIDTH - 1, y);

        drawScanner(s, SCREEN_WIDTH * 4 / 5 - 16 - 32, y + 16);


        if (s.bStation) {
            graphics.setColor(Color.white);

            x = SCREEN_WIDTH * 4 / 5 - 16 - 32;
            y = SCREEN_HEIGHT - 40;

            graphics.fillRect(x, y, 32, 4);
            graphics.fillRect(x, y + 16, 32, 4);
            graphics.fillRect(x, y + 32, 32, 4);

            graphics.fillRect(x, y, 4, 16);
            graphics.fillRect(x + 28, y + 16, 4, 16);
        }


        // Laser sights
        if (s.iView > 0 && s.iView < 5) {
            int cx = SCREEN_CEN_X,
                    cy = SCREEN_CEN_Y;


            switch (s.LaserType[s.iView - 1]) {
                case 0:
                    graphics.setColor(Color.white);
                    graphics.drawLine(cx, cy + 4, cx, cy + 12);
                    graphics.drawLine(cx, cy - 4, cx, cy - 12);
                    graphics.drawLine(cx + 4, cy, cx + 12, cy);
                    graphics.drawLine(cx - 4, cy, cx - 12, cy);
                    break;

                case 1:
                    graphics.setColor(Color.white);
                    graphics.drawLine(cx + 4, cy + 4, cx + 12, cy + 12);
                    graphics.drawLine(cx + 4, cy - 4, cx + 12, cy - 12);
                    graphics.drawLine(cx - 4, cy + 4, cx - 12, cy + 12);
                    graphics.drawLine(cx - 4, cy - 4, cx - 12, cy - 12);
                    break;

                case 2:
                    graphics.setColor(Color.white);
                    graphics.drawLine(cx - 12, cy - 12, cx - 12 + 4, cy - 12);
                    graphics.drawLine(cx - 12, cy - 12, cx - 12, cy - 12 + 4);

                    graphics.drawLine(cx + 12, cy - 12, cx + 12 - 4, cy - 12);
                    graphics.drawLine(cx + 12, cy - 12, cx + 12, cy - 12 + 4);

                    graphics.drawLine(cx - 12, cy + 12, cx - 12 + 4, cy + 12);
                    graphics.drawLine(cx - 12, cy + 12, cx - 12, cy + 12 - 4);

                    graphics.drawLine(cx + 12, cy + 12, cx + 12 - 4, cy + 12);
                    graphics.drawLine(cx + 12, cy + 12, cx + 12, cy + 12 - 4);
                    break;

                case 3:
                    graphics.setColor(Color.white);
                    graphics.drawLine(cx - 12, cy - 12, cx - 12 + 4, cy - 12);
                    graphics.drawLine(cx - 12, cy - 12, cx - 12, cy - 12 + 4);

                    graphics.drawLine(cx + 12, cy - 12, cx + 12 - 4, cy - 12);
                    graphics.drawLine(cx + 12, cy - 12, cx + 12, cy - 12 + 4);

                    graphics.drawLine(cx - 12, cy + 12, cx - 12 + 4, cy + 12);
                    graphics.drawLine(cx - 12, cy + 12, cx - 12, cy + 12 - 4);

                    graphics.drawLine(cx + 12, cy + 12, cx + 12 - 4, cy + 12);
                    graphics.drawLine(cx + 12, cy + 12, cx + 12, cy + 12 - 4);

                    graphics.drawLine(cx, cy + 4, cx, cy + 12);
                    graphics.drawLine(cx, cy - 4, cx, cy - 12);
                    graphics.drawLine(cx + 4, cy, cx + 12, cy);
                    graphics.drawLine(cx - 4, cy, cx - 12, cy);
                    break;
            }
        }      */
    }

    private static void drawStatBox(Canvas canvas, float x, float y, float cur, float max) {
        float width = 64;
        float height = 10;

        Paint rectPaint = new Paint();
        rectPaint.setColor(Color.YELLOW);
        rectPaint.setStyle(Paint.Style.STROKE);
        canvas.drawRect(x - 2, y, width + x - 2, y + height - 1, rectPaint);

        if (max != 0) {
            float scaledValC = cur * (width - 3) / max;
            if (scaledValC != 0) {
                if (scaledValC > (width - 3))
                    scaledValC = width - 3;

                rectPaint = new Paint();
                if (scaledValC > width / 4) {
                    rectPaint.setColor(Color.GREEN);
                } else if (scaledValC > width / 8) {
                    rectPaint.setColor(Color.YELLOW);
                } else {
                    rectPaint.setColor(Color.RED);
                }

                canvas.drawRect(x, y + 2, x + scaledValC, y + height - 2, rectPaint);
            }
        }
    }

    private static void drawStatBox(Canvas canvas, float x, float y, float cur, float tar, float max) {
        int width = 64;
        int height = 10;

        x += (width / 2) - 2;

        Paint paint = new Paint();
        paint.setColor(Color.YELLOW);
        paint.setStyle(Paint.Style.STROKE);

        canvas.drawRect(x - width / 2, y, x + width / 2, y + height - 1, paint);
        canvas.drawLine(x, y - 2, x, y + height + 1, paint);

        if (max == 0) {
            return;
        }

        int ScaledValC = (int) (cur * (width / 2 - 2) / max);
        if (ScaledValC != 0) {
            paint = new Paint();
            paint.setColor(Color.GREEN);
            if (ScaledValC > 0) {
                canvas.drawRect(x, y + 2, x + ScaledValC, y + height - 2, paint);
            } else {
                canvas.drawRect(x + ScaledValC, y + 2, x, y + height - 2, paint);
            }
        }

        int ScaledValT = (int) (tar * (width / 2 - 2) / max);
        paint = new Paint();
        paint.setColor(Color.RED);
        canvas.drawLine(x + ScaledValT, y - 2, x + ScaledValT, y + height + 1, paint);
    }

    private void drawRadar(ShipSim shipPlayer, float x, float y) {
        int Width = 160,
                Height = Width / 2,
                Scale = 500, //100,
                lim = Scale * Width / 2;


        /*graphics.setColor(Color.yellow);

        drawO(x, y, Width, Height);
        drawO(x, y, (int) (Width * .75), (int) (Height * .75));
        drawO(x, y, (int) (Width * .5), (int) (Height * .5));
        drawO(x, y, (int) (Width * .25), (int) (Height * .25));

        // Do radar relative to ship, not view
        Matrix43 mat = new Matrix43(shipPlayer.Mat);
        Vector3D v = new Vector3D();
        mat.affineInverse();

        if (pobjList != null) {
            Obj3d s = (Obj3d) pobjList.Next;
            while (s != null) {
                v.copy(s.Position);
                v.sub(shipPlayer.Position);

                if (v.size() < lim) {
                    v.mul(mat);

                    int x2 = (int) (x + v.x / Scale),
                            y2 = (int) (y - v.z / (Scale * 2));

                    graphics.setColor(s.colour);

                    graphics.drawLine(x2, y2, x2, (int) (y2 - v.y / (Scale * 2)));
                    graphics.fillRect(x2, (int) (y2 - v.y / (Scale * 2)), 3, 3);
                }

                s = (Obj3d) s.Next;
            }
        }

        Obj p = universe.Bodies.Next;
        while (p != null) {
            if (p.Type == Universe.OBJ_STATION) {
                Station station = (Station) p;

                v.copy(station.Position);
                v.sub(shipPlayer.Position);
                if (v.size() < lim) {
                    v.mul(mat);

                    int x2 = (int) (x + v.x / Scale),
                            y2 = (int) (y - v.z / (Scale * 2));

                    graphics.setColor(station.colour);

                    graphics.drawLine(x2, y2, x2, (int) (y2 - v.y / (Scale * 2)));
                    graphics.fillRect(x2, (int) (y2 - v.y / (Scale * 2)), 3, 3);
                }
            }
            p = p.Next;
        }     */
    }

    private void drawO(int x, int y, int w, int h) {
        //graphics.drawOval(x - w / 2, y - h / 2, w, h);
    }

    private void drawScanner(ShipSim shipPlayer, int x, int y) {
        int Size = 32;

        /*graphics.setColor(Color.yellow);
        graphics.drawRect(x, y, Size, Size);
        graphics.drawOval(x, y, Size, Size);

        graphics.fillRect(x + Size / 2, y + Size / 2, 1, 1);
        graphics.fillRect(x + Size / 4, y + Size / 2, 1, 1);
        graphics.fillRect(x + 3 * Size / 4, y + Size / 2, 1, 1);
        graphics.fillRect(x + Size / 2, y + Size / 4, 1, 1);
        graphics.fillRect(x + Size / 2, y + 3 * Size / 4, 1, 1);


        Matrix43 mat = new Matrix43(shipPlayer.Mat);
        mat.affineInverse();


        Vector3D v = new Vector3D();

        if (shipPlayer.bStation) {
            v.copy(universe.station.Position);        //Naughty
            v.sub(shipPlayer.Position);
            v.mul(mat);

            if (v.z < 0) {
                graphics.setColor(Color.lightGray);
                v.z = -v.z;
            } else
                graphics.setColor(Color.white);
        } else {
            v.copy(shipPlayer.currentPlanet.Position);
            v.sub(shipPlayer.Position);
            v.mul(mat);

            if (v.z < 0) {
                graphics.setColor(Color.red);
                v.copy(universe.sun.Position);            //Naughty
                v.sub(shipPlayer.Position);
                v.mul(mat);
            } else
                graphics.setColor(Color.green);
        }

        float x2 = Misc.atan(v.x, v.z),
                y2 = Misc.atan(-v.y, v.z);

        x2 = (float) (x2 / Math.PI * Size);
        y2 = (float) (y2 / Math.PI * Size);

        graphics.fillRect(x + Size / 2 + ((int) x2) - 1, y + Size / 2 + ((int) y2) - 1, 3, 3); */
    }

    public static boolean fovClip(Vector3D Pos) {
        boolean occlude = false;
        if (Pos.z < GameCon.PERSPECTIVE_H / 2)
            occlude = true;
        else if (Pos.z > 30000)
            occlude = true;
        else if (Pos.x < -1.5 * Pos.z)
            occlude = true;
        else if (Pos.x > 1.5 * Pos.z)
            occlude = true;
        else if (Pos.y < -1.2 * Pos.z)
            occlude = true;
        else if (Pos.y > 1.2 * Pos.z) occlude = true;
        return (occlude);
    }

    public ShipSim getPlayer() {
        return player;
    }

    public float getScreenWidth() {
        return screenWidth;
    }

    public float getScreenHeight() {
        return screenHeight;
    }

    public float getScreenCenterX() {
        return screenCenterX;
    }

    public float getScreenCenterY() {
        return screenCenterY;
    }

    private void addToFps() {
        long drawTime = System.currentTimeMillis() - lastScreenDrawTime;
        if (MAX_GAME_SPEED_MS - drawTime > 0) {
            try {
                Thread.sleep(MAX_GAME_SPEED_MS - drawTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (System.currentTimeMillis() - fpsTime > 1000) {
            fps = currFps;
            currFps = 0;
            fpsTime = System.currentTimeMillis();
        }

        lastScreenDrawTime = System.currentTimeMillis();
        currFps++;
    }
}