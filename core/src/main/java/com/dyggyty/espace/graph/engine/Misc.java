package com.dyggyty.espace.graph.engine;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;

/**
 * Misc useful methods that require no class dependencies                                                                        *
 * Cough, except, cough, standard, cough, API, c c c cough.
 */
public class Misc {

    private Misc() {
    }

    public static float moveTo(float Cur, float Max, float Delta) {
        if (Delta != 0) {
            if (Delta > 0) {
                Cur += Delta;
                if (Cur > Max)
                    Cur = Max;
            } else {
                Cur += Delta;
                if (Cur < -Max)
                    Cur = -Max;
            }
        }

        return (Cur);
    }

    public static float moveToTarget(float Cur, float Tar, float Speed) {
        if (Cur != Tar) {
            if (Cur < Tar) {
                Cur += Speed;
                if (Cur > Tar)
                    Cur = Tar;
            } else {
                Cur -= Speed;
                if (Cur < Tar)
                    Cur = Tar;
            }
        }

        return (Cur);
    }

    public static void drawStringCentre(Canvas canvas, Paint paint, String s, float y) {
        Paint textPaint = new Paint(paint);
        textPaint.setTextAlign(Paint.Align.CENTER);
        float xPos = (canvas.getWidth() / 2);

        canvas.drawText(s, xPos, y, textPaint);
    }

    public static void drawStringRight(Canvas canvas, Paint paint, String s, float x, float y) {
        float[] widths = new float[s.length()];
        paint.getTextWidths(s, widths);
        float width = 0;
        for (float charWidth : widths) {
            width += charWidth;
        }
        canvas.drawText(s, x - width, y, paint);
    }

    public static int SQR(int x) {
        return (x * x);
    }

    public static float SQR(float x) {
        return (x * x);
    }

    public static float atan(float opp, float adj) {
        float ang;

        if (adj == 0) {
            if (opp == 0)
                ang = 0;
            else if (opp >= 0)
                ang = (float) Math.PI / 2;
            else
                ang = (float) -Math.PI / 2;
        } else {
            if (adj > 0) {
                ang = (float) Math.atan(opp / adj);
            } else {
                ang = (float) (Math.PI - Math.atan(-opp / adj));
                if (ang > (float) Math.PI)
                    ang -= (float) (Math.PI * 2);
            }
        }

        return (ang);
    }

    /**
     * Draw polygon
     *
     * @param canvas The canvas to draw on
     * @param color  Integer representing a fill color (see http://developer.android.com/reference/android/graphics/Color.html)
     * @param points Polygon corner points
     */
    public static void drawPoly(Canvas canvas, int color, Point[] points) {
        // line at minimum...
        if (points.length < 2) {
            return;
        }

        // paint
        Paint polyPaint = new Paint();
        polyPaint.setColor(color);
        polyPaint.setStyle(Paint.Style.FILL);

        // path
        Path polyPath = new Path();
        polyPath.moveTo(points[0].x, points[0].y);
        int i, len;
        len = points.length;
        for (i = 0; i < len; i++) {
            polyPath.lineTo(points[i].x, points[i].y);
        }
        polyPath.lineTo(points[0].x, points[0].y);

        // draw
        canvas.drawPath(polyPath, polyPaint);
    }
}