package com.dyggyty.espace.graph.engine;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import com.dyggyty.espace.game.GameCon;

/**
 * General purpose polygon primative class
 */
public class Poly {
    public Poly prev;
    public Poly next;

    public int x[] = new int[Face.MAX_V];
    public int y[] = new int[Face.MAX_V];

    public int iNSides;

    public int color;

    public Poly() {
        iNSides = 0;
        prev = null;
        next = null;
    }

    public void linkTo(Poly p) {
        if (this.next != null || this.prev != null) this.unlink();
        if (p == null) return;

        this.next = p.next;
        this.prev = p;
        p.next = this;

        if (this.next != null)
            this.next.prev = this;
    }

    public void unlink() {
        if (this.prev != null) {
            this.prev.next = this.next;
        }

        if (this.next != null) {
            this.next.prev = this.prev;
        }

        this.next = null;
        this.prev = null;
    }

    public static void drawOrderTable(Canvas canvas, Poly orderTable[], Poly polyFree) {
        if (GameCon.bFilled) {
            for (int i = GameCon.NUM_Z_BUCKETS - 1; i > 0; i--) {
                Poly p = orderTable[i].next,
                        pNext;

                while (p != null) {
                    pNext = p.next;

                    Paint paint = new Paint();
                    paint.setColor(p.color);
                    paint.setStyle(Paint.Style.FILL);

                    switch (p.iNSides) {
                        case 1: {
                            canvas.drawCircle(p.x[0] - p.x[1] / 2, p.y[0] - p.y[1] / 2, p.x[1], paint);
                            break;
                        }

                        case 3:
                        case 4:
                        default: {
                            if (p.x[1] < 3 & p.y[1] < 3) {
                                canvas.drawRect(p.x[0] - p.x[1] / 2, p.y[0] - p.y[1] / 2, p.x[1], p.y[1], paint);
                            } else {
                                Point[] points = new Point[p.iNSides];

                                for (int point = 0; point < p.iNSides; point++) {
                                    points[point] = new Point(p.x[point], p.y[point]);
                                }
                                Misc.drawPoly(canvas, p.color, points);
                            }
                            break;
                        }
                    }

                    p.linkTo(polyFree);
                    p = pNext;
                }
            }
        } else {
            for (int i = GameCon.NUM_Z_BUCKETS - 1; i > 0; i--) {
                Poly p = orderTable[i].next,
                        pNext;

                while (p != null) {
                    pNext = p.next;

                    Paint paint = new Paint();
                    paint.setColor(p.color);

                    switch (p.iNSides) {
                        case 1: {
                            if (p.x[1] < 3 & p.y[1] < 3) {
                                canvas.drawRect(p.x[0] - p.x[1] / 2, p.y[0] - p.y[1] / 2, p.x[1], p.y[1], paint);
                            } else {
                                canvas.drawCircle(p.x[0] - p.x[1] / 2, p.y[0] - p.y[1] / 2, p.x[1], paint);
                            }
                            break;
                        }

                        case 3:
                        case 4:
                        default: {
                            Point[] points = new Point[p.iNSides];

                            for (int point = 0; point < p.iNSides; point++) {
                                points[point] = new Point(p.x[point], p.y[point]);
                            }
                            Misc.drawPoly(canvas, p.color, points);
                            break;
                        }
                    }

                    p.linkTo(polyFree);
                    p = pNext;
                }
            }
        }
    }
}

