package com.dyggyty.espace.graph.engine;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import com.dyggyty.espace.game.GameCon;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * Simple 3d model class.
 */
public class Model3d {
    private int iNFaces,
            iNVecs;

    private BufferedInputStream bufferIn;

    private Vector<Face> faces = new Vector<Face>();
    private Vector<Vector3D> vecs = new Vector<Vector3D>();

    // Bounds
    public Vector3D Max = new Vector3D();
    public Vector3D Min = new Vector3D();

    // File loading state statics
    private static final int LOAD_NOTHING = 0,
            LOAD_NVERTS = 1,
            LOAD_NFACES = 2,
            LOAD_FACES = 3,
            LOAD_VERTS = 4,
            LOAD_RGB = 5,
            LOAD_NORMAL = 6,
            LOAD_END = 10;

    public Model3d() {
        iNFaces = 0;
        iNVecs = 0;
    }

    public void addVec(float x, float y, float z) {
        vecs.addElement(new Vector3D(x, y, z));
        iNVecs++;
    }

    public void addFace(int v0, int v1, int v2, int v3) {
        faces.addElement(new Face(v0, v1, v2, v3));
        iNFaces++;
    }

    public void addFace(int color, Vector3D normal, int i[]) {
        Face f = new Face(i);

        f.normal.copy(normal);
        f.setColor(color);

        faces.addElement(f);
        iNFaces++;
    }


    public void printInfo(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);

        canvas.drawText("NVerts:" + iNVecs, 10, 40, paint);
        canvas.drawText("NFaces:" + iNFaces, 10, 50, paint);
    }

    public int nfaces() {
        return (iNFaces);
    }

    public Vector3D vec(int i) {
        return vecs.elementAt(i);
    }

    public Vector3D vec(int i, int j) {
        return vecs.elementAt(face(i).v[j]);
    }

    public Face face(int i) {
        return faces.elementAt(i);
    }

    public boolean load(String filename) {
        boolean flag;

        flag = true;

        try {
            URL url = getClass().getClassLoader().getResource(filename);
            bufferIn = new BufferedInputStream(url.openStream());
        } catch (IOException e) {
            System.out.println("IOE" + e);
        }


        if (flag) {
            int iC,
                    iState = LOAD_NOTHING,
                    iNumsReq = 0,
                    iNumsCur = 0;

            int i[] = new int[16];
            float f[] = new float[16];

            char c;

            int r = 0, g = 0, b = 0;
            Vector3D normal = new Vector3D();

            String str;

            try {
                boolean flag2 = true;
                do {
                    str = "";

                    while (true) {
                        iC = bufferIn.read();
                        if (iC == -1) break;

                        if (iC != 10 && iC != 13) {
                            c = (char) iC;
                            str += c;
                        } else {
                            if (iC == 10) break;
                        }
                    }

                    StringTokenizer st = new StringTokenizer(str, ", \t");

                    while (st.hasMoreTokens()) {
                        String strCom = st.nextToken();

                        if (strCom.charAt(0) >= '0' && strCom.charAt(0) <= '9' ||
                                strCom.charAt(0) == '-') {
                            Float Flt = new Float(strCom);
                            // Integer	Int	=	new Integer(strCom);
                            f[iNumsCur] = Flt.floatValue();
                            i[iNumsCur] = Flt.intValue();

                            if (iNumsCur == 0 && iState == LOAD_FACES) {
                                iNumsReq = i[0] + 1;
                            }

                            // Numerical value
                            iNumsCur++;

                            if (iNumsCur == iNumsReq) {
                                switch (iState) {
                                    case LOAD_NVERTS:
                                        break;
                                    case LOAD_NFACES:
                                        break;
                                    case LOAD_VERTS:
                                        addVec((float) i[0], (float) i[1], (float) i[2]); // Elite ships
                                        break;

                                    case LOAD_RGB:
                                        r = i[0];
                                        g = i[1];
                                        b = i[2];

                                        iNumsReq = 3;
                                        iState = LOAD_NORMAL;

                                        break;

                                    case LOAD_NORMAL:
                                        normal.x = f[0];
                                        normal.y = f[1];
                                        normal.z = f[2];

                                        iNumsReq = 4;
                                        iState = LOAD_FACES;
                                        break;

                                    case LOAD_FACES:
                                        int col = Color.rgb(r, g, b);
                                        addFace(col, normal, i);

                                        iNumsReq = 3;
                                        iState = LOAD_RGB;

                                        break;
                                }

                                iNumsCur = 0;
                            }

                        } else {
                            // New command
                            if (strCom.compareTo("NVERTS") == 0) {
                                iState = LOAD_NVERTS;
                                iNumsReq = 1;
                            } else if (strCom.compareTo("NFACES") == 0) {
                                iState = LOAD_NFACES;
                                iNumsReq = 1;
                            } else if (strCom.compareTo("VERTEX") == 0) {
                                iState = LOAD_VERTS;
                                iNumsReq = 3;
                            } else if (strCom.compareTo("FACES") == 0) {
                                iState = LOAD_RGB;
                                iNumsReq = 3;
                            } else if (strCom.compareTo("END") == 0) {
                                iState = LOAD_END;
                                iNumsReq = 0;
                            } else
                                iState = LOAD_NOTHING;

                            iNumsCur = 0;
                        }
                    }

                    if (iC == -1)
                        flag2 = false;
                    else if (str.length() != 0) {
                        if (str == "END")
                            flag2 = false;
                    }
                } while (flag2);
            } catch (IOException e) {
                System.out.println("IOException:" + e);
            }

            // Create bounding box
            for (int iV = 0; iV != iNVecs; iV++) {
                if (iV == 0) {
                    Min.x = Max.x = vec(iV).x;
                    Min.y = Max.y = vec(iV).y;
                    Min.z = Max.z = vec(iV).z;
                } else {
                    if (vec(iV).x < Min.x) Min.x = vec(iV).x;
                    if (vec(iV).y < Min.y) Min.y = vec(iV).y;
                    if (vec(iV).z < Min.z) Min.z = vec(iV).z;

                    if (vec(iV).x > Max.x) Max.x = vec(iV).x;
                    if (vec(iV).y > Max.y) Max.y = vec(iV).y;
                    if (vec(iV).z > Max.z) Max.z = vec(iV).z;
                }
            }

            //	Might as well create some Face normals
            Vector3D v[] = new Vector3D[2];

            for (int iF = 0; iF != iNFaces; iF++) {
                v[0] = new Vector3D(vec(iF, 1).x - vec(iF, 0).x,
                        vec(iF, 1).y - vec(iF, 0).y,
                        vec(iF, 1).z - vec(iF, 0).z);

                v[1] = new Vector3D(vec(iF, 2).x - vec(iF, 0).x,
                        vec(iF, 2).y - vec(iF, 0).y,
                        vec(iF, 2).z - vec(iF, 0).z);

                v[0].normalise();
                v[1].normalise();
                v[0].mul(v[1]);

                v[0].normalise();    // Shouldn't be needed - but due to rounding errors, makes the absolute world of difference

                System.out.println(v[0].x + "," + v[0].y + "," + v[0].z + "===="
                        + face(iF).normal.x + ","
                        + face(iF).normal.y + ","
                        + face(iF).normal.z + ",");
            }
        }

        return (flag);
    }


    // Draw object into order table
    public void render(Poly orderTable[], Poly polyFree, Vector3D light, Matrix43 m) {
        Matrix43 m2 = new Matrix43();
        Vector3D v = new Vector3D(),
                v2 = new Vector3D();

        // Can now use m2 for lighting purposes
        m2.copy(m);
        m2.trans(0, 0, 0);

        v2.set(m.m[3][0], m.m[3][1], m.m[3][2]);
        int iot = (int) (v2.size() / 10);
        if (iot < 1) iot = 1;
        if (iot >= GameCon.NUM_Z_BUCKETS) iot = GameCon.NUM_Z_BUCKETS - 1;

        for (int count = 0; count != this.iNFaces; count++) {
            int i = this.iNFaces - count - 1;

            Poly p = polyFree.next;

            if (p == null)
                break;
            else {
                boolean drawFlag = true;
                int iNSides;

                iNSides = this.face(i).num_v;

// Normal clip*********************
// transform normal
                v2.copy(this.face(i).normal);
                v2.mul(m2);
// transform point on Face
                v.copy(this.vec(i, 0));
                v.mul(m);
                v.normalise();

                float d = v.dot(v2);
// d	=	-1;

                if (d < 0 && iNSides > 2) {
                    int iOT = 0;

                    for (int j = 0; j != iNSides; j++) {
                        v.copy(this.vec(i, j));

                        v.mul(m);

                        if (v.z < (GameCon.PERSPECTIVE_H / 2)) {
                            // -ve z check; i.e. should clip with z=0 (or similar)
                            drawFlag = false;
                            break;
                        }

                        iOT += v.pers();

                        p.x[j] = v.sX;
                        p.y[j] = v.sY;
                    }

                    if (drawFlag) {
                        iOT /= iNSides;

                        v.copy(this.face(i).normal);
                        v.mul(m2);

                        int Intensity = ((int) (Math.acos(v.dot(light)) * (224f / Math.PI))) + 31,
                                r, g, b;

                        if (Color.red(this.face(i).getColor()) == 0
                                && Color.green(this.face(i).getColor()) == 0
                                && Color.blue(this.face(i).getColor()) == 0) {
                            p.color = Color.rgb(Intensity, Intensity, Intensity);
                        } else {
                            r = (Color.red(this.face(i).getColor()) * Intensity) / 255;
                            g = (Color.green(this.face(i).getColor()) * Intensity) / 255;
                            b = (Color.blue(this.face(i).getColor()) * Intensity) / 255;

                            p.color = Color.rgb(r, g, b);
                        }
                        p.iNSides = iNSides;

                        iOT = iot;

                        p.linkTo(orderTable[iOT]);
                    }
                }
            }
        }
    }


    public void renderBounds(Canvas canvas, Matrix43 m) {
        Vector3D av[] = new Vector3D[8];
        Point[] points = new Point[4];

        av[0] = new Vector3D(Min.x, Min.y, Min.z);
        av[1] = new Vector3D(Max.x, Min.y, Min.z);
        av[2] = new Vector3D(Min.x, Min.y, Max.z);
        av[3] = new Vector3D(Max.x, Min.y, Max.z);

        av[4] = new Vector3D(Min.x, Max.y, Min.z);
        av[5] = new Vector3D(Max.x, Max.y, Min.z);
        av[6] = new Vector3D(Min.x, Max.y, Max.z);
        av[7] = new Vector3D(Max.x, Max.y, Max.z);

        av[0].mul(m);
        av[1].mul(m);
        av[2].mul(m);
        av[3].mul(m);
        av[4].mul(m);
        av[5].mul(m);
        av[6].mul(m);
        av[7].mul(m);

        av[0].pers();
        av[1].pers();
        av[2].pers();
        av[3].pers();
        av[4].pers();
        av[5].pers();
        av[6].pers();
        av[7].pers();

        points[0] = new Point(av[0].sX, av[0].sY);
        points[1] = new Point(av[1].sX, av[1].sY);
        points[2] = new Point(av[3].sX, av[3].sY);
        points[3] = new Point(av[2].sX, av[2].sY);

        Misc.drawPoly(canvas, Color.CYAN, points);

        points[0] = new Point(av[4].sX, av[4].sY);
        points[1] = new Point(av[5].sX, av[5].sY);
        points[2] = new Point(av[7].sX, av[7].sY);
        points[3] = new Point(av[6].sX, av[6].sY);
        Misc.drawPoly(canvas, Color.CYAN, points);

        Paint paint = new Paint();
        paint.setColor(Color.CYAN);

        canvas.drawLine(av[0].sX, av[0].sY, av[4].sX, av[4].sY, paint);
        canvas.drawLine(av[1].sX, av[1].sY, av[5].sX, av[5].sY, paint);
        canvas.drawLine(av[3].sX, av[3].sY, av[7].sX, av[7].sY, paint);
        canvas.drawLine(av[2].sX, av[2].sY, av[6].sX, av[6].sY, paint);
    }
}

