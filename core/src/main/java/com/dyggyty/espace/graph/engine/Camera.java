package com.dyggyty.espace.graph.engine;

import com.dyggyty.espace.game.model.ShipSim;
import com.dyggyty.espace.graph.Obj3d;

public class Camera extends Obj3d {
    private ShipSim target;

    public void target(ShipSim tar) {
        target = tar;
    }

    public void pos(Vector3D v) {
        movement.copy(v);
        movement.sub(Position);
        Position.copy(v);
    }

    public void ang(Vector3D v) {
        Angle.copy(v);
    }

    public void copy(Camera cam)    // Just enough for rendering purposes
    {
        Position.copy(cam.Position);
        Angle.copy(cam.Angle);
        Mat.copy(cam.Mat);
    }

    public void update(ShipSim s) {
        target = s;
        update();
    }

    public void update() {
        Matrix43 m = new Matrix43();
        Matrix43 m2 = new Matrix43();

        pos(target.Position);

        if (target.iView == 2) {
            Vector3D v = new Vector3D(0f, 1f, 0f);
            m.copy(target.Mat);
            v.mul(m);
            m2.rotateAbout(v, (float) (Math.PI));
            m.mul(m2);
            Mat.affineInverse(m);
        } else if (target.iView == 3) {
            Vector3D v = new Vector3D(0f, 1f, 0f);
            m.copy(target.Mat);
            v.mul(m);
            m2.rotateAbout(v, (float) (-Math.PI / 2));
            m.mul(m2);
            Mat.affineInverse(m);
        } else if (target.iView == 4) {
            Vector3D v = new Vector3D(0f, 1f, 0f);
            m.copy(target.Mat);
            v.mul(m);
            m2.rotateAbout(v, (float) (Math.PI / 2));
            m.mul(m2);
            Mat.affineInverse(m);
        } else
            Mat.affineInverse(target.Mat);
    }
}

