package com.dyggyty.espace.graph.engine;

import android.graphics.Color;

/**
 * Face data class
 */
public class Face {
    public final static int MAX_V = 8;

    public int num_v;
    public int v[] = new int[MAX_V];
    private int color;

    Vector3D normal = new Vector3D();

    public Face() {
        int i;

        num_v = 0;

        for (i = 0; i != MAX_V; i++)
            v[i] = 0;

        color = Color.BLACK;
    }

    public Face(int v0, int v1, int v2, int v3) {
        v[0] = v0;
        v[1] = v1;
        v[2] = v2;
        v[3] = v3;
    }

    public Face(int v2[]) {
        num_v = v2[0];
        if (num_v > MAX_V) num_v = MAX_V;

        for (int i = 0; i != num_v; i++) {
            v[i] = v2[i + 1];
        }
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}

