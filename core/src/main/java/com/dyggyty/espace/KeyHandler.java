package com.dyggyty.espace;

import com.dyggyty.espace.game.GameCon;
import com.dyggyty.espace.game.model.ShipSim;
import com.dyggyty.espace.graph.engine.Misc;
import java.util.Arrays;

/**
 * Misc keyboard handling
 */
public class KeyHandler {
    public enum Action {
        FIRE(0, 0, 0, 0), ACCELERATION(0, 0, 0, 0), SLOWDOWN(0, 0, 0, 0), SELL(0, 0, 0, 0), JUMP(0, 0, 0, 0),
        HYPER(0, 0, 0, 0), DOCK(0, 0, 0, 0), TARGET(0, 0, 0, 0), UNARM(0, 0, 0, 0), MISSILE(0, 0, 0, 0),
        TOUCH(0, 0, Integer.MAX_VALUE, Integer.MAX_VALUE);

        private int leftX;
        private int topY;
        private int rightX;
        private int bottomY;

        private Action(int leftX, int topY, int rightX, int bottomY) {
            this.leftX = leftX;
            this.topY = topY;
            this.rightX = rightX;
            this.bottomY = bottomY;
        }

        public int getLeftX() {
            return leftX;
        }

        public int getTopY() {
            return topY;
        }

        public int getRightX() {
            return rightX;
        }

        public int getBottomY() {
            return bottomY;
        }
    }

    private static final KeyHandler instance = new KeyHandler();

    private static final int MIN_PITCH = 5;
    private static final int MAX_PITCH = 40;
    private static final int MIN_ROLL = 10;
    private static final int MAX_ROLL = 30;
    private static final int ZERO_ROLL = 40;

    private Action action;
    private boolean keyDown;

    private float azimuth;
    private float pitch;
    private float roll;

    public int iView;

    private KeyHandler() {
        iView = 1;
    }

    public static KeyHandler getInstance() {
        return instance;
    }

    public Action getAction() {
        Action currentAction = action;
        if (!keyDown) {
            action = null;
        }
        return currentAction;
    }

    public Action getCompletedAction() {
        if (!keyDown) {
            return getAction();
        }

        return null;
    }

    public void processKeyPress(int x, int y, Action... actions) {
        Action[] actionsToProcess = Arrays.copyOfRange(actions, 0, actions.length);
        Arrays.sort(actionsToProcess);

        for (Action action : actions) {
            if (x >= action.getLeftX() && y >= action.getTopY() && x <= action.getRightX() && y <= action.getBottomY()) {
                this.action = action;
                break;
            }
        }

        keyDown = true;
    }

    public float getAzimuth() {
        return azimuth;
    }

    public void setAzimuth(float azimuth) {
        this.azimuth = azimuth;
    }

    public float getPitch() {
        return pitch;
    }

    public void setPitch(float pitch) {
        if (Math.abs(pitch) < MIN_PITCH) {
            this.pitch = 0;
        } else if (pitch > MAX_PITCH) {
            this.pitch = MAX_PITCH;
        } else if (pitch < -MAX_PITCH) {
            this.pitch = -MAX_PITCH;
        } else {
            this.pitch = pitch;
        }
    }

    public float getRoll() {
        return roll;
    }

    public void setRoll(float roll) {
        this.roll = roll + ZERO_ROLL;
        if (Math.abs(this.roll) < MIN_ROLL) {
            this.roll = 0;
        } else if (this.roll > MAX_ROLL) {
            this.roll = MAX_ROLL;
        } else if (this.roll < -MAX_ROLL) {
            this.roll = -MAX_ROLL;
        }
    }

    public void processKeyUp() {
        keyDown = false;
    }

    public void Down(int key) {
        if (key >= '0' && key <= '9') {
            if (GameCon.CURRENT_MODE == GameCon.MODE_DOCKED) {
                if (key == '0' || key >= '4')
                    iView = key - '0';
                else if (key == '1')
                    GameCon.CURRENT_MODE = GameCon.MODE_LAUNCH;
            } else
                iView = key - '0';
        }

        if (GameCon.CURRENT_MODE == GameCon.MODE_HYPER)
            return;

        switch (key) {
/*
            case 'a':
                iFire |= 1;
                break;
            case '/':
                iDec |= 1;
                break;
            case '-':
                iSell |= 1;
                break;

            case 'j':
                iJump += 1;
                break;

            case 'c':
                iDock |= 1;
                break;

            case 't':
                iTarget |= 1;
                break;
            case 'u':
                iUnarm |= 1;
                break;
            case 'm':
                iMissile |= 1;
                break;

            case ' ':
                iAcc |= 1;
                break;
*/

            case 'q':
                if (GameCon.CURRENT_MODE != GameCon.MODE_TITLE)
                    GameCon.CURRENT_MODE = GameCon.MODE_AUTODOCK2;
                break;

            case 'h':
                if (GameCon.CURRENT_MODE == GameCon.MODE_SPACE) {
                    //iHyper |= 1;
                    break;
                }

            case 'f':
                if (GameCon.bFilled)
                    GameCon.bFilled = false;
                else
                    GameCon.bFilled = true;
                break;
            case 'p':
                if (GameCon.bPart)
                    GameCon.bPart = false;
                else
                    GameCon.bPart = true;
                break;
            case 'b':
                if (GameCon.bBounding)
                    GameCon.bBounding = false;
                else
                    GameCon.bBounding = true;
                break;
        }
    }

    public void processKeys(ShipSim s) {
        s.iView = this.iView;

        float downSpeed = -roll / MAX_ROLL;
        if (downSpeed > 0) {
            if (s.Rtar.x < 0) {
                s.Rtar.x = 0;
            } else {
                s.Rtar.x = Misc.moveTo(s.Rtar.x, s.Rmax.x, downSpeed * s.Radjust.x);
            }
        } else if (downSpeed < 0) {
            if (s.Rtar.x > 0) {
                s.Rtar.x = 0;
            } else {
                s.Rtar.x = Misc.moveTo(s.Rtar.x, s.Rmax.x, downSpeed * s.Radjust.x);
            }
        } else {
            s.Rtar.x = Misc.moveToTarget(s.Rtar.x, 0, s.Rdampen.x);
        }

        float rotationSpeed = -pitch / MAX_PITCH;
        if (rotationSpeed > 0) {
            if (s.Rtar.z < 0) {
                s.Rtar.z = 0;
            } else {
                s.Rtar.z = Misc.moveTo(s.Rtar.z, s.Rmax.z, rotationSpeed * s.Radjust.z);
            }
        } else if (rotationSpeed < 0) {
            if (s.Rtar.z > 0) {
                s.Rtar.z = 0;
            } else {
                s.Rtar.z = Misc.moveTo(s.Rtar.z, s.Rmax.z, rotationSpeed * s.Radjust.z);
            }
        } else {
            s.Rtar.z = Misc.moveToTarget(s.Rtar.z, 0, s.Rdampen.z);
        }

/*
        if (iAcc != 0) {
            s.fSpeedTar = Misc.moveTo(s.fSpeedTar, s.fSpeedMax, 2);
        } else if (iDec != 0) {
            s.fSpeedTar = Misc.moveToTarget(s.fSpeedTar, 0, 2);
        }

        if (s.iView > 0 && s.iView < 5) {
            if (iFire != 0 && s.LaserType[s.iView - 1] != -1)
                s.bFire = true;
            else
                s.bFire = false;
        }

        if (iMissile != 0) {
            s.bMissileFire = true;
        } else {
            s.bMissileFire = false;
        }

        if (iHyper != 0) {
            if (GameCon.CURRENT_MODE == GameCon.MODE_SPACE) {
                if (s.iCurrentPlanet != s.iSelectedPlanet) {
                    float Dist = s.selectedPlanet.distanceFrom(s.currentPlanet);

                    if (Dist < s.Fuel) {
                        s.Fuel -= Dist;
                        GameCon.CURRENT_MODE = GameCon.MODE_HYPER;
                        GameCon.TIME_IN_MODE = 0;
                    }
                }
            }
        }

        if (s.NumMissiles != 0) {
            if (iTarget != 0) {
                if (s.MissileState == 0) s.MissileState = 1;
            }

            if (iUnarm != 0) {
                if (s.MissileState != 0) s.MissileState = 0;
            }
        }
*/
    }
}
