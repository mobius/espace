package com.dyggyty.espace;

import com.dyggyty.espace.game.GameCon;
import com.dyggyty.espace.game.model.Planet;
import com.dyggyty.espace.game.model.ShipSim;
import com.dyggyty.espace.game.model.Station;
import com.dyggyty.espace.game.model.Sun3d;
import com.dyggyty.espace.game.model.Universe;
import com.dyggyty.espace.graph.Obj3d;
import com.dyggyty.espace.graph.engine.Render;
import com.dyggyty.espace.graph.engine.Vector3D;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.Event;
import java.awt.event.MouseEvent;
import java.applet.Applet;

public class JElite extends Applet implements Runnable {

    boolean gameInit = true,
            gameRunning = true;

    //Render render = new Render();	// This is going to do all the drawing

    // Globals
    Thread rungame;
    int demoModel = 0;
    int demoMods[] = {8, 17, 23, 28, 21, 16, 14, 15, 9, 2, 0, 1, -1};

    Universe uni = new Universe();

    //KeyHandler keys = new KeyHandler();

    // Misc
    int OldView;

    // Once off initialisations
    public void init() {
        repaint();
        initialise();
        //render.initialise(uni);

        gameInit = false;
    }

    public void initialise() {

        uni.initStaticLists();
        uni.loadModels();

        GameCon.CURRENT_MODE = GameCon.MODE_TITLE;
        GameCon.OLD_MODE = GameCon.MODE_START;
        GameCon.TIME_IN_MODE = 0;

        setBackground(new Color(0x102030));
    }

    private void initTitle() {
        uni.clearLists(true);

        uni.Cam.Mat.unit();
        uni.Cam.Mat.trans(0f, 0f, 0f);
        uni.Cam.Position.set(0f, 0f, 0f);

        ShipSim s = uni.getShip();

        if (s != null) {
            s.defaults(GameCon.CobraIII);
            demoModel = 0;
            s.PlayerCraft = false;
            s.pos(0, 0, -2800);
            s.mod(uni.Model[demoMods[demoModel]]);
            s.linkTo(uni.ShipUsed);
            s.ang(0, 0, 0);
            s.Rcur.set(0f, 0f, 0f);
            s.Rtar.set((float) (Math.PI / 70), (float) (Math.PI / 120), (float) (Math.PI / 82));
        }

        //render.setupList(null);
    }

    private void initGame() {
        uni.clearLists(true);
        uni.initialiseGalaxy();

        // Setup player ships
        for (int i = 0; i != GameCon.NUM_PLAYERS; i++) {
            ShipSim s = uni.getShip();
            if (s != null) {
                uni.ShipPlayer[i] = s;
                s.linkTo(uni.ShipUsed);

                s.currentPlanet = uni.planets[s.iCurrentPlanet];
                s.selectedPlanet = uni.planets[s.iSelectedPlanet];
                s.PlayerCraft = true;
                s.PlayerId = i;

                // Zero rotations
                s.Rtar.zero();
                s.Rcur.zero();
                s.Mat.unit();
            } else {
                System.out.println("Couldn't allocate player ship");
                System.exit(1);
            }

        }

        //render.setupList(uni.ShipPlayer[0]);
    }

    private void initSpace(int type) {
        ShipSim s = uni.ShipPlayer[0];
        Vector3D off;

        uni.clearLists(false);

        s.currentPlanet = uni.planets[s.iCurrentPlanet];

        Planet planet = s.currentPlanet;
        Sun3d sun = uni.sun;
        Station station = uni.station;

        station.setup(planet);
        station.mod(uni.Model[11]);
        sun.setup(s.iCurrentPlanet);

        if (type == 0) {
            off = new Vector3D(uni.station.Position);
            off.z += 4000;
        } else
            off = new Vector3D(0, 0, 0);

        s.mod(uni.Model[0]);
        s.Position.copy(off);
        s.Mat.unit();
        s.fSpeedTar = 10;
        s.fSpeedCur = s.fSpeedMax;
        s.Position.z -= 3000;
        s.PlayerCraft = true;
        s.PlayerId = 0;
        s.MissileState = 0;
        s.iView = 1;
        //keys.iView = 1;

        planet.linkTo(uni.Bodies);
        station.linkTo(uni.Bodies);
        sun.linkTo(uni.Bodies);

        uni.Cam.update(s);
        //render.setupList(s);
    }


    public void start() {
        rungame = new Thread(this);
        rungame.start();
    }

    public void stop() {
        gameRunning = false;
    }

    public void run() {
        ShipSim s;

        do {

            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
            }

            if (GameCon.CURRENT_MODE != GameCon.OLD_MODE) {
                GameCon.OLD_MODE = GameCon.CURRENT_MODE;
                GameCon.TIME_IN_MODE = 0;
            } else
                GameCon.TIME_IN_MODE++;

            switch (GameCon.CURRENT_MODE) {
                case GameCon.MODE_TITLE:
                    if (GameCon.TIME_IN_MODE == 0) {
                        initTitle();
                    } else {
                        uni.Cam.Position.z += 100;
                        s = (ShipSim) uni.ShipUsed.Next;
                        if (s != null) {
                            int t = GameCon.TIME_IN_MODE % 700;
                            float off;

                            s.run(uni);

                            if (t == 0)
                                s.randomColour();

                            if (t < 100)
                                off = (100 - t) * 150;
                            else if (t > 600)
                                off = (t - 600) * 150;
                            else
                                off = 0;

                            if (t == 0) {
                                demoModel = demoModel + 1;
                                if (demoMods[demoModel] == -1)
                                    demoModel = 0;
                                s.mod(uni.Model[demoMods[demoModel]]);
                                System.out.println("Displaying ship model " + demoMods[demoModel]);
                            }

                            s.Position.copy(uni.Cam.Position);
                            s.Position.z = uni.Cam.Position.z + 500 + off * 2;
                        }

/*
                        if (keys.iAcc != 0) {
                            keys.iAcc = 0;
                            GameCon.CURRENT_MODE = GameCon.MODE_START;
                        }
*/
                    }
                    break;

                case GameCon.MODE_START:
                    initGame();
                    GameCon.CURRENT_MODE = GameCon.MODE_DOCKED;
                    break;

                case GameCon.MODE_DOCKED:
                 /*   keys.processKeys(uni.ShipPlayer[0]);
                    if (GameCon.TIME_IN_MODE == 0) {
                        uni.ShipPlayer[0].iView = 9;
                        keys.iView = 9;
                        OldView = keys.iView;
                    } else {
                        if (OldView != keys.iView) {
                            if (keys.iView == 4) {
                                uni.EquipState = 0;
                                uni.EquipItem = -1;
                            }
                        }

                        OldView = keys.iView;
                    }*/
                    break;

                case GameCon.MODE_LAUNCH:
                    if ((GameCon.TIME_IN_MODE / 50) > 0) {
                        initSpace(0);
                        GameCon.CURRENT_MODE = GameCon.MODE_SPACE;
                    }
                    break;

                case GameCon.MODE_SPACE:
                    //keys.processKeys(uni.ShipPlayer[0]);

                    // Run ship handlers
                    s = (ShipSim) uni.ShipUsed.Next;
                    while (s != null) {
                        ShipSim sNext = s.Next();
                        s.run(uni);
                        s = sNext;
                    }


/*
                    if (keys.iDock != 0) {
                        keys.iDock = 0;

                        if (uni.ShipPlayer[0].autoEngaged()) {
                            uni.ShipPlayer[0].autoStop();
                        } else {
                            if (uni.ShipPlayer[0].Upgrade[6] && uni.ShipPlayer[0].bStation) {
                                uni.ShipPlayer[0].followStation(uni.station);
                            }
                        }
                    }

                    if (keys.iJump == 1 && !uni.ShipPlayer[0].bStation) {
                        uni.ShipPlayer[0].jump((Obj3d) uni.ShipUsed.Next);
                    }
*/

                    uni.event();

                    uni.Cam.update();
                    break;

                case GameCon.MODE_HYPER:
                    if ((GameCon.TIME_IN_MODE / 50) > 0) {
                        GameCon.CURRENT_MODE = GameCon.MODE_SPACE;

                        // Set new system
                        uni.ShipPlayer[0].iCurrentPlanet = uni.ShipPlayer[0].iSelectedPlanet;
                        initSpace(1);
                    }
                    break;

                case GameCon.MODE_DOCK:
                    if (GameCon.TIME_IN_MODE > 100) {
                        uni.ShipPlayer[0].autoStop();
                        uni.ShipPlayer[0].MissileState = 0;
                        GameCon.CURRENT_MODE = GameCon.MODE_DOCKED;
                    }
                    break;

                case GameCon.MODE_AUTODOCK1:
                    break;

                case GameCon.MODE_AUTODOCK2:
                    GameCon.CURRENT_MODE = GameCon.MODE_TITLE;
                    GameCon.TIME_IN_MODE = 0;
                    break;
            }


            if (gameRunning)  {
                //render.rePaint(uni.Cam, uni.ShipUsed);
            }

        } while (gameRunning);
    }

    public void paint(Graphics g) {
/*
        if (render != null) {
            if (Render.offscreen != null) {
                g.drawImage(Render.offscreen, 0, 0, this);
            }
        }
*/
    }

    /**
     * Модернизация перегрузки, чтобы позволить легкое двойное буферирование.
     *
     * @param g Graphics.
     */
    public void update(Graphics g) {
        paint(g);
    }

    // Implement applet io methods
    public void processMouseEvent(MouseEvent event) {
        uni.MouseX = event.getX();
        uni.MouseY = event.getY();
    }

    public boolean mouseMove(Event ev, int x, int y) {
        uni.MouseX = x;
        uni.MouseY = y;
        return true;
    }

    public boolean mouseDrag(Event ev, int x, int y) {
        uni.MouseX = x;
        uni.MouseY = y;
        uni.MouseClick = true;
        return true;
    }

    public boolean mouseDown(Event ev, int x, int y) {
        uni.MouseClick = true;
        return true;
    }

    public boolean mouseUp(Event ev, int x, int y) {
        uni.MouseClick = false;
        return true;
    }

    public boolean keyDown(Event ev, int key) {
        //keys.Down(key);
        return true;
    }

    public boolean keyUp(Event ev, int key) {
        //keys.Up(key);
        return true;
    }
}


