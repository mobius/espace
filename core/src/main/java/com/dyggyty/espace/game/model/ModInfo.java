package com.dyggyty.espace.game.model;

public class ModInfo {
    // Model info/data
    public String Name;

    public int Type;
    public int BaseOccupation;
    public int ActualOccupation;
    public int Alignment;

    public ModInfo() {
        Name = null;

        Type = 0;
        BaseOccupation = 0;
        ActualOccupation = 0;
        Alignment = 0;
    }
}

