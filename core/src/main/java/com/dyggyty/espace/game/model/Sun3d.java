package com.dyggyty.espace.game.model;

import android.graphics.Color;
import com.dyggyty.espace.graph.Obj3d;
import java.util.Random;

/**
 * Class for most Planet handling - including Planet types, galactic position, etc.
 */
public class Sun3d extends Obj3d {
    // Local random number gen variables for Planet creation
    Random Rand = new Random();

    public Sun3d() {
        Type = Universe.OBJ_SUN;
    }

    public Sun3d(int iSeed) {
        Type = Universe.OBJ_SUN;
        setup(iSeed);
    }

    public void setup(int iSeed) {
        Rand.setSeed(iSeed);
        Position.z = -200000;    //	 This is really hyperspace dependant
        Size = 10000 + rand(10) * 10000;
        this.colour = Color.YELLOW;
    }

    private int rand(int iMax) {
        int iR = (Math.abs(Rand.nextInt())) % (iMax);
        return (iR);
    }
}


