package com.dyggyty.espace.game.model;

import com.dyggyty.espace.game.GameCon;
import com.dyggyty.espace.graph.Obj3d;
import com.dyggyty.espace.graph.engine.Matrix43;
import com.dyggyty.espace.graph.engine.Misc;
import com.dyggyty.espace.graph.engine.Part;
import com.dyggyty.espace.graph.engine.Vector3D;

import java.awt.Graphics;
import java.awt.Color;

/**
 * Space ship sim class
 */
public class ShipSim extends Obj3d {
    public Vector3D Rcur = new Vector3D();
    public Vector3D Rmax = new Vector3D();
    public Vector3D Rtar = new Vector3D();

    public Vector3D Radjust = new Vector3D();
    public Vector3D Rdampen = new Vector3D();

    public float fSpeedCur;
    public float fSpeedMax;
    public float fSpeedTar;

    public boolean PlayerCraft;
    public int PlayerId;

    public boolean bFire,
            bShoot,
            bMissileFire;
    public ShipSim sBest;
    public boolean bStation;

    public float FrontShields,
            FrontShieldsMax,
            RearShields,
            RearShieldsMax,
            EnergyBank[] = new float[4],
            EnergyBankMax,
            EnergyRechargeRate,
            LaserTemp,
            CabinTemp,
            Fuel;

    public int LaserType[] = new int[4],
            FiringRate;

    public int NumMissiles,
            MissileState;

    public Obj3d MissileTarget;

    public boolean Upgrade[] = new boolean[8];

    public int iView;

    public int iCurrentPlanet,
            iSelectedPlanet;

    public Planet currentPlanet,
            selectedPlanet;

    public int Cargo[] = new int[GameCon.NUM_PRODUCTS];
    public int CargoSize;

    public int Credits;
    public int PoliceStatus;


    //	Auto-pilot/simple AI
    private int iAutoState;
    private int iTimeInState;

    private ShipSim shipTar;
    private Station stationTar;

    public static final int AI_DO_NOTHING = 0;
    public static final int AI_FOLLOW_SHIP = 0x100;
    public static final int AI_ATTACK_SHIP = 0x200;
    public static final int AI_FLY_AWAY = 0x300;
    public static final int AI_DOCK = 0x400;

    public ShipSim() {
        Next = Prev = null;
        defaults(0);
    }

    public ShipSim Next() {
        return ((ShipSim) this.Next);
    }

    public void defaults(int iShipType) {
        Position.zero();
        Angle.zero();
        renderPos.zero();
        Rtar.zero();
        Rcur.zero();
        Mat.unit();

        // General ship-specific setup
        switch (iShipType) {
            case GameCon.Missile:
                fSpeedMax = 200;

                Rmax.x = (float) (Math.PI / 60);
                Rmax.y = (float) (Math.PI / 60);
                Rmax.z = (float) (Math.PI / 60);

                Radjust.x = (float) (Rmax.x / 10);
                Radjust.y = (float) (Rmax.y / 10);
                Radjust.z = (float) (Rmax.z / 10);

                Rdampen.x = (float) (Rmax.x / 20);
                Rdampen.y = (float) (Rmax.y / 20);
                Rdampen.z = (float) (Rmax.z / 20);
                break;

            case GameCon.CobraIII:
                FrontShieldsMax = 100;
                RearShieldsMax = 100;
                EnergyRechargeRate = 1;

                LaserType[0] = 0;
                LaserType[1] = -1;
                LaserType[2] = -1;
                LaserType[3] = -1;
                fSpeedMax = 60;

                Rmax.x = (float) (Math.PI / 100);
                Rmax.y = (float) (Math.PI / 100);
                Rmax.z = (float) (Math.PI / 100);

                Radjust.x = (float) (Rmax.x / 10);
                Radjust.y = (float) (Rmax.y / 10);
                Radjust.z = (float) (Rmax.z / 10);

                Rdampen.x = (float) (Rmax.x / 20);
                Rdampen.y = (float) (Rmax.y / 20);
                Rdampen.z = (float) (Rmax.z / 20);
                break;

            default:
                FrontShieldsMax = 100;
                RearShieldsMax = 100;
                EnergyRechargeRate = 1;

                LaserType[0] = 0;
                LaserType[1] = -1;
                LaserType[2] = -1;
                LaserType[3] = -1;
                fSpeedMax = 40;

                Rmax.x = (float) (Math.PI / 100);
                Rmax.y = (float) (Math.PI / 100);
                Rmax.z = (float) (Math.PI / 100);

                Radjust.x = (float) (Rmax.x / 10);
                Radjust.y = (float) (Rmax.y / 10);
                Radjust.z = (float) (Rmax.z / 10);

                Rdampen.x = (float) (Rmax.x / 20);
                Rdampen.y = (float) (Rmax.y / 20);
                Rdampen.z = (float) (Rmax.z / 20);

                break;
        }

//
        iSelectedPlanet = 60;
        if (iSelectedPlanet > GameCon.NUM_PLANETS)
            iSelectedPlanet = GameCon.NUM_PLANETS - 1;
        iCurrentPlanet = iSelectedPlanet;

        Fuel = 7;

        CargoSize = 30 * 1000000;
        Credits = 1000; // * 10000;
        PoliceStatus = 0;

        for (int i = 0; i != GameCon.NUM_PRODUCTS; i++)
            Cargo[i] = 0;

        FrontShields = FrontShieldsMax;
        RearShields = RearShieldsMax;
        EnergyBankMax = 100;
        EnergyBank[0] = EnergyBankMax;
        EnergyBank[1] = EnergyBankMax;
        EnergyBank[2] = EnergyBankMax;
        EnergyBank[3] = EnergyBankMax;

        LaserTemp = 0;
        CabinTemp = 0;
        FiringRate = 0;

        fSpeedCur = 0;
        fSpeedTar = 0;

        bFire = false;
        bMissileFire = false;
        sBest = null;

        NumMissiles = 3;
        MissileState = 0;

        for (int i = 0; i != 8; i++)
            Upgrade[i] = false;

        Upgrade[6] = true;

        randomColour();


        iAutoState = AI_DO_NOTHING;
        iTimeInState = 0;
        shipTar = null;
        stationTar = null;
    }

    public void AIdock(Station s) {
        if (s == null)
            return;
        iAutoState = AI_DOCK;
        iTimeInState = 0;
        stationTar = s;
    }

    public void AIfollow(ShipSim s) {
        if (s == null)
            return;
        AIChangeState(AI_FOLLOW_SHIP, s);
    }

    public void AIattack(ShipSim s) {
        if (s == null)
            return;
        AIChangeState(AI_ATTACK_SHIP, s);
    }

    public void AIChangeState(int NEW_STATE, ShipSim s) {
        iAutoState = NEW_STATE;
        iTimeInState = 0;
        shipTar = s;
    }

    public void run(Universe uni) {
        Matrix43 m = new Matrix43();

        Vector3D thrust = new Vector3D();

        if (GameCon.bPart && !PlayerCraft) {
            Part p = (Part) uni.partFree.Next;
            if (p != null) {
                p.linkTo(uni.PartUsed);
                p.setup(Mat, Position);
            }
        }

        // Energy replenishment
        if (PlayerCraft)
            recharge(EnergyRechargeRate);

        if (PlayerCraft) {
            if (iAutoState != 0) {
                switch (iAutoState) {
                    case 1:
                        followShip();
                        break;

                    case 2:
                        followStation();
                        break;
                }
            }
        } else {
            switch (iAutoState) {
                case AI_DO_NOTHING:
                    break;

                case AI_DOCK:
                    followStation();
                    break;

                case AI_FOLLOW_SHIP:
                    followShip();
                    break;

                case AI_ATTACK_SHIP:
                    float Dist = followShip();
                    if (Dist < GameCon.SHIP_RANGE / 2) {
                        if (iTimeInState > 400 && Dist < GameCon.SHIP_RANGE / 24) {
                            AIChangeState(AI_FLY_AWAY, shipTar);
                        } else {
                            // Consider firing?
                            if ((iTimeInState & 0xf) == 0) {
                                bFire = true;
                            }
                        }
                    }
                    break;

                case AI_FLY_AWAY:
                    if (iTimeInState < 400) {
                        Vector3D vecTar = new Vector3D(Position);

                        vecTar.sub(shipTar.Position);
                        vecTar.add(Position);

                        fSpeedTar = fSpeedMax * 0.8f;
                        followVec(vecTar);
                    } else {
                        AIChangeState(AI_ATTACK_SHIP, shipTar);
                    }

                    break;
            }

            iTimeInState++;
        }

        Rcur.x = Misc.moveToTarget(Rcur.x, Rtar.x, Rdampen.x);
        Rcur.y = Misc.moveToTarget(Rcur.y, Rtar.y, Rdampen.y);
        Rcur.z = Misc.moveToTarget(Rcur.z, Rtar.z, Rdampen.z);

        // Want z rotation first - due to autopilot/AI requirements
        if (Rcur.z != 0) {
            thrust.set(0f, 0f, 1f);
            thrust.mul(Mat);
            m.rotateAbout(thrust, -Rcur.z);
            Mat.mul(m);
        }

        if (Rcur.x != 0) {
            thrust.set(1f, 0f, 0f);
            thrust.mul(Mat);
            m.rotateAbout(thrust, -Rcur.x);
            Mat.mul(m);
        }

        if (Rcur.y != 0) {
            thrust.set(0f, 1f, 0f);
            thrust.mul(Mat);
            m.rotateAbout(thrust, -Rcur.y);
            Mat.mul(m);
        }

        // Ensure minimum speed for ships
        if (fSpeedTar < 4)
            fSpeedTar = 4;
        fSpeedCur = Misc.moveToTarget(fSpeedCur, fSpeedTar, 2f);
        if (fSpeedCur != 0) {
            thrust.set(0f, 0f, (float) fSpeedCur);
            thrust.mul(Mat);
            Position.add(thrust);
            movement.copy(thrust);
        } else
            movement.set(0f, 0f, 0f);


        // ****************
        // Handle weaponary
        // ****************
        Vector3D laserDir = new Vector3D();
        int View;

        if (!PlayerCraft) {
            laserDir.set(0f, 0f, 1f);
            View = 1;
        } else {
            View = iView;
            switch (iView) {
                case 1:
                    laserDir.set(0f, 0f, 1f);
                    break;
                case 2:
                    laserDir.set(0f, 0f, -1f);
                    break;
                case 3:
                    laserDir.set(-1f, 0f, 0f);
                    break;
                case 4:
                    laserDir.set(1f, 0f, 0f);
                    break;
            }
        }


        bShoot = false;

        // Handle laser firing
        if (bFire) {
            if (FiringRate > 0)
                FiringRate--;

            if (FiringRate == 0) {
                float Heat = ((float) GameCon.LaserHeat[LaserType[View - 1]]) / 4;

                if (LaserTemp < 100 - Heat) {
                    LaserTemp = Misc.moveToTarget(LaserTemp, 100, Heat);
                    FiringRate = GameCon.LaserRate[LaserType[View - 1]];
                    bShoot = true;
                }
            }
        }

        if (!bShoot)
            LaserTemp = Misc.moveToTarget(LaserTemp, 0, 0.1f);    // Cool down

        if (bShoot || MissileState == 1) {
            // Let's see if we're pointing at another ship?
            Vector3D dis = new Vector3D();
            float BestDist = Float.MAX_VALUE,
                    Dist;
            int BestColValue = 0;

            sBest = null;

            laserDir.mul(Mat);

            ShipSim s = (ShipSim) uni.ShipUsed.Next;
            while (s != null) {
                if (s != this) {
                    dis.copy(Position);
                    dis.sub(s.Position);
                    Dist = dis.size();

                    if (Dist < BestDist) {
                        int ColValue = s.collideWithVec(Position, laserDir);

                        if (ColValue != 0) {
                            BestDist = Dist;
                            sBest = s;
                            BestColValue = ColValue;
                        }
                    }
                }

                s = s.Next();
            }

            if (sBest != null) {
                // Target aquired

                if (MissileState == 1) {
                    MissileState = 2;
                    MissileTarget = sBest; //(obj3d) sBest;
                }

                if (bShoot) {
                    if (!sBest.damage(BestColValue, GameCon.LaserDamage[LaserType[View - 1]])) {
                        if (sBest.PlayerCraft)
                            GameCon.CURRENT_MODE = GameCon.MODE_TITLE;
                        else
                            sBest.linkTo(uni.ShipFree);    // Perhaps, have an exploding ship list - Mmm.
                    }
                }
            }
        }


        if (bMissileFire && MissileState == 2) {
            // Launch missile at enemy
            ShipSim s = (ShipSim) uni.ShipFree.Next;
            if (s != null) {
                NumMissiles--;
                MissileState = 0;
                s.defaults(GameCon.Missile);
                s.followShip((ShipSim) MissileTarget);
                s.Position.copy(Position);
                s.Mat.copy(Mat);
                s.mod(uni.Model[2]);
                s.fSpeedCur = s.fSpeedMax;
                s.linkTo(uni.ShipUsed);
            }
        }


        // Are we near the space station? Then do bodged ship
        Vector3D stat = new Vector3D(uni.station.Position);
        stat.sub(Position);

        bStation = stat.size() < Station.RANGE;

        if (stat.size() < 200) {
            if (PlayerCraft)
                GameCon.CURRENT_MODE = GameCon.MODE_DOCK;
            else
                linkTo(uni.ShipFree);
        }
    }

    public boolean damage(int ColValue, float Damage) {
        if (ColValue == 1) {
            if (FrontShields > 0) {
                FrontShields -= Damage;

                if (FrontShields < 0) {
                    Damage = -FrontShields;
                    FrontShields = 0;
                } else
                    Damage = 0;
            }
        } else {
            if (RearShields > 0) {
                RearShields -= Damage;

                if (RearShields < 0) {
                    Damage = -RearShields;
                    RearShields = 0;
                } else
                    Damage = 0;
            }
        }

        if (Damage > 0) {
            for (int Bank = 3; Bank >= 0; Bank--) {
                if (EnergyBank[Bank] > 0) {
                    EnergyBank[Bank] -= Damage;

                    if (EnergyBank[Bank] < 0) {
                        Damage = -EnergyBank[Bank];
                        EnergyBank[Bank] = 0;
                    } else
                        Damage = 0;
                }

                if (Damage == 0) break;
            }
        }

        boolean Alive;
        if (EnergyBank[0] != 0)
            Alive = true;
        else
            Alive = false;

        return (Alive);
    }

    public void recharge(float Energy) {
        if (Energy > 0) {
            for (int Bank = 0; Bank <= 3; Bank++) {
                if (EnergyBank[Bank] < EnergyBankMax) {
                    EnergyBank[Bank] += Energy;

                    if (EnergyBank[Bank] > EnergyBankMax) {
                        Energy = EnergyBank[Bank] - EnergyBankMax;
                        EnergyBank[Bank] = EnergyBankMax;
                    } else
                        Energy = 0;
                }

                if (Energy == 0) break;
            }
        }

        if (Energy != 0) {
            if (FrontShields < RearShields) {
                FrontShields += Energy;
                if (FrontShields > FrontShieldsMax)
                    FrontShields = FrontShieldsMax;
            } else {
                RearShields += Energy;
                if (RearShields > RearShieldsMax)
                    RearShields = RearShieldsMax;
            }
        }
    }

    public void jump(Obj3d s) {
        boolean jump = true;

        Vector3D v = new Vector3D();

        while (s != null) {
            if (s != this) {
                v.sub(s.Position, this.Position);

                if (v.size() <= GameCon.SHIP_RANGE) {
                    jump = false;
                    break;
                }
            }

            s = (Obj3d) s.Next;
        }

        if (jump) {
            Vector3D thrust = new Vector3D(0f, 0f, 5000f);
            thrust.mul(Mat);
            Position.add(thrust);
        }
    }

    public void autoStop() {
        iAutoState = 0;
    }

    public boolean autoEngaged() {
        return (iAutoState != 0);
    }

    public void followShip(ShipSim s) {
        if (s == null) return;
        iAutoState = 1;
        shipTar = s;
    }

    public void followStation(Station s) {
        if (s == null) return;
        iAutoState = 2;
        stationTar = s;
    }


    private float followShip() {
        float Dist;
        Vector3D vecTar = new Vector3D(shipTar.Position);

        Dist = followVec(vecTar);
        return (Dist);
    }

    private void followStation() {
        Vector3D vecTar = new Vector3D(stationTar.Position);
        followVec(vecTar);
    }

    private float followVec(Vector3D vecTar) {
        Matrix43 mat = new Matrix43(Mat);

        mat.affineInverse();
        vecTar.sub(this.Position);

        float Dist = vecTar.size();

        vecTar.mul(mat);

        float ang;

        if (vecTar.y >= 0)
            ang = Misc.atan(vecTar.x, vecTar.y);
        else
            ang = -Misc.atan(vecTar.x, -vecTar.y);

        Rtar.z = ang / 4;

        if (Rtar.z > Rmax.z)
            Rtar.z = Rmax.z;
        else if (Rtar.z < -Rmax.z)
            Rtar.z = -Rmax.z;

        if (Math.abs(ang) < Math.PI / 4 || Math.abs(ang) > Math.PI * 3 / 4) {
            float ang2 = Misc.atan(vecTar.y, vecTar.z);

            Rtar.x = ang2 / 6;

            if (Rtar.x > Rmax.x) {
                Rtar.x = Rmax.x;
            } else if (Rtar.x < -Rmax.x) {
                Rtar.x = -Rmax.x;
            }
        }
        if (Dist < 4000) {
            fSpeedTar = fSpeedMax * 0.25f;
        } else {
            fSpeedTar = fSpeedMax * 0.6f;
        }

        return (Dist);
    }

    public void debugInfo(Graphics g, Vector3D vecTarO, int x, int y) {
        Vector3D vecTar = new Vector3D(vecTarO);
        Matrix43 mat = new Matrix43(Mat);

        mat.affineInverse();
        vecTar.sub(this.Position);
        vecTar.mul(mat);

        float ang2 = Misc.atan(vecTar.y, vecTar.z);

        g.setColor(Color.green);
        g.drawString("X:" + vecTar.x, x, y);
        g.drawString("Y:" + vecTar.y, x, y + 16);
        g.drawString("Z:" + vecTar.z, x, y + 32);
        g.drawString("Ang2:" + ang2, x, y + 48);
    }
}