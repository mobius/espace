package com.dyggyty.espace.game.model;

//	Universe class
//
// Contains:
//		All drawable models
//		planets for current galaxy
//		ships in current system
//		planets in current system
//		space stations in current system

import com.dyggyty.espace.game.GameCon;
import com.dyggyty.espace.graph.Obj;
import com.dyggyty.espace.graph.engine.Camera;
import com.dyggyty.espace.graph.engine.Matrix43;
import com.dyggyty.espace.graph.engine.Model3d;
import com.dyggyty.espace.graph.engine.Part;
import com.dyggyty.espace.graph.engine.Vector3D;

public class Universe {
    public final static int OBJ_NONE = 0;
    public static final int OBJ_STATION = 1;
    public final static int OBJ_PLANET = 2;
    public final static int OBJ_SUN = 3;
    public final static int OBJ_SHIP = 4;

    // Character types
    public final static int C_TRADER = 0,
            C_MERCENARY = 1,
            C_PIRATE = 2,
            C_POLICE = 3,
            C_TRANSPORT = 4,
            C_MINER = 5,
            C_SCAVENGER = 6,
            C_KILLER = 7;

    public final static int CT_TRADER = 1 << C_TRADER,
            CT_MERCENARY = 1 << C_MERCENARY,
            CT_PIRATE = 1 << C_PIRATE,
            CT_POLICE = 1 << C_POLICE,
            CT_TRANSPORT = 1 << C_TRANSPORT,
            CT_MINER = 1 << C_MINER,
            CT_SCAVENGER = 1 << C_SCAVENGER,
            CT_KILLER = 1 << C_KILLER;

    public final static String CT_NAME[] = new String[16];

    public int CurrentGalaxy;

    public Model3d Model[] = new Model3d[GameCon.NUM_3D_OBJS];

    // Lists of objects
    public Obj ShipFree = new Obj();            // Free ship pool.
    public Obj ShipUsed = new Obj();            // Used ship list.
    public Obj Bodies = new Obj();            //	List of stations/suns/planets in current system

    public Part partFree = new Part();
    public Part PartUsed = new Part();

    // Actual objects
    public Camera Cam = new Camera();

    public ShipSim ShipPlayer[] = new ShipSim[GameCon.NUM_PLAYERS];
    public Planet planets[] = new Planet[GameCon.NUM_PLANETS];
    public Part part[] = new Part[GameCon.NUM_PARTS];

    public Station station = new Station();
    public Sun3d sun = new Sun3d();

    // Mouse/menu control variables - probably shouldn't be here, but what the hell.
    public int MouseX = 0,
            MouseY = 0;
    public boolean MouseClick = false;

    public int EquipState,
            EquipItem;

    // Event handling
    int iTimeSinceLastEvent;

    public Universe() {
        CurrentGalaxy = 0;
        iTimeSinceLastEvent = 0;

        CT_NAME[C_TRADER] = "Trader";
        CT_NAME[C_MERCENARY] = "Mercernary";
        CT_NAME[C_PIRATE] = "Pirate";
        CT_NAME[C_POLICE] = "Police";
        CT_NAME[C_TRANSPORT] = "Transporter";
        CT_NAME[C_MINER] = "Miner";
        CT_NAME[C_SCAVENGER] = "Scavenger";
        CT_NAME[C_KILLER] = "Killer";
    }

    public void initStaticLists() {
        for (int i = 0; i != GameCon.NUM_PLANETS; i++)
            planets[i] = new Planet();

        for (int i = 0; i != GameCon.NUM_3D_OBJS; i++)
            Model[i] = new Model3d();

        for (int i = 0; i != GameCon.NUM_SHIPS; i++) {
            ShipSim s = new ShipSim();
            s.linkTo(ShipFree);
        }

        for (int i = 0; i != GameCon.NUM_PARTS; i++) {
            Part p = new Part();
            p.linkTo(partFree);
        }

    }


    public void initialiseGalaxy() {
        int CG = CurrentGalaxy * 10000;

        System.out.println("Creating planets");
        for (int i = 0; i != GameCon.NUM_PLANETS; i++)
            planets[i].setup(i + CG);
        System.out.println("Finished planets");
    }


    public void loadModels() {
        int i;

        i = 0;
        while (GameCon.modInfo[i].Name != null) {
            Model[i].load(GameCon.modInfo[i].Name + ".dat");
            i++;
        }
    }

    public void clearLists(boolean ClearPlayer) {
        // Clear used ship list - except player ships
        Obj s = ShipUsed.Next,
                sNext;

        while (s != null) {
            sNext = s.Next;
            if ((ClearPlayer || !((ShipSim) s).PlayerCraft)
                    && s.Type != OBJ_STATION) {
                s.linkTo(ShipFree);
            }
            s = sNext;
        }

        s = Bodies.Next;
        while (s != null) {
            sNext = s.Next;
            s.unlink();
            s = sNext;
        }

        s = PartUsed.Next;
        while (s != null) {
            sNext = s.Next;
            s.linkTo(partFree);
            s = sNext;
        }

        iTimeSinceLastEvent = 0;
    }

    public ShipSim getShip() {
        return ((ShipSim) ShipFree.Next);
    }

    public void event() {
        boolean eventOccurred = false;

        if (iTimeSinceLastEvent > 300) {
            ShipSim s = getShip();
            if (s != null) {
                Matrix43 mat = new Matrix43(),
                        mat2 = new Matrix43();
                Vector3D off = new Vector3D(0, 0, 40000);

                s.defaults(GameCon.CobraIII);

                int ship;
                do {
                    ship = (int) (Math.abs(Math.random() * GameCon.MAX_MODELS));
                } while (GameCon.modInfo[ship].BaseOccupation == 0);

                System.out.println("Created ship model: " + GameCon.modInfo[ship].Name);

                s.mod(Model[ship]);
                s.Position.copy(ShipPlayer[0].Position);
                mat2.rotY((float) (Math.random() * Math.PI));
                mat.rotZ((float) (Math.random() * Math.PI));
                mat.mul(mat2);
                off.mul(mat);
                s.Position.add(off);

                // Устанавливает поведение компьютера к кораблю игрока
                double job = Math.abs(Math.random());

                if (job > 0.6) {
                    s.AIdock(station); //Naughty, relying on single station
                } else if (job > 0.4) {
                    s.AIfollow(ShipPlayer[0]);
                } else {
                    s.AIattack(ShipPlayer[0]);
                }
                s.linkTo(ShipUsed);
            }

            eventOccurred = true;
        }

        if (eventOccurred)
            iTimeSinceLastEvent = 0;
        else
            iTimeSinceLastEvent++;
    }
}
 