package com.dyggyty.espace.game.model;

import android.graphics.Color;
import com.dyggyty.espace.game.GameCon;
import com.dyggyty.espace.graph.Obj3d;
import com.dyggyty.espace.graph.engine.Matrix43;
import com.dyggyty.espace.graph.engine.Vector3D;

/**
 * Space station
 */
public class Station extends Obj3d {
    public int price[] = new int[GameCon.NUM_PRODUCTS];
    public int numAvailable[] = new int[GameCon.NUM_PRODUCTS];

    public final static int AGRI = 1 << 0;
    public final static int INDU = 1 << 1;
    public final static int GOOD = 1 << 2;
    public final static int BAD = 1 << 3;
    public final static int LOTECH = 1 << 4;
    public final static int HITECH = 1 << 5;

    public final static int RANGE = 100000;

    public final static int avPrice[] = new int[GameCon.NUM_PRODUCTS];
    public final static int perWeight[] = new int[GameCon.NUM_PRODUCTS];
    public final static int priceModifiers[] = new int[GameCon.NUM_PRODUCTS];

    public Station() {
        Type = Universe.OBJ_STATION;

        colour = Color.LTGRAY;
        Col = 7;

        priceModifiers[0] = AGRI;        //	Food
        priceModifiers[1] = AGRI;    //	Textiles
        priceModifiers[2] = AGRI + HITECH;        //	Radioactives
        priceModifiers[3] = AGRI + BAD;     //	Slaves
        priceModifiers[4] = AGRI;    //	Liquor/Wines
        priceModifiers[5] = AGRI;        //	Luxuries
        priceModifiers[6] = AGRI + BAD;        //	Narcotics
        priceModifiers[7] = INDU + HITECH;        //	Computers
        priceModifiers[8] = INDU;        //	Machinery
        priceModifiers[9] = AGRI + HITECH;        //	Alloys
        priceModifiers[10] = BAD;        //	Firearms
        priceModifiers[11] = AGRI + GOOD;        //	Furs
        priceModifiers[12] = AGRI;        //	Minerals
        priceModifiers[13] = GOOD;        //	Gold
        priceModifiers[14] = GOOD;        //	Platinum
        priceModifiers[15] = GOOD;       //	Gem-stones
        priceModifiers[16] = BAD;        //	Alien Items

        avPrice[0] = 44;
        avPrice[1] = 64;
        avPrice[2] = 212;
        avPrice[3] = 80;
        avPrice[4] = 252;
        avPrice[5] = 912;
        avPrice[6] = 1148;
        avPrice[7] = 840;
        avPrice[8] = 564;
        avPrice[9] = 328;
        avPrice[10] = 704;
        avPrice[11] = 560;
        avPrice[12] = 80;
        avPrice[13] = 372;
        avPrice[14] = 652;
        avPrice[15] = 164;
        avPrice[16] = 270;

        perWeight[0] = 1000000;
        perWeight[1] = 1000000;
        perWeight[2] = 1000000;
        perWeight[3] = 1000000;
        perWeight[4] = 1000000;
        perWeight[5] = 1000000;
        perWeight[6] = 1000000;
        perWeight[7] = 1000000;
        perWeight[8] = 1000000;
        perWeight[9] = 1000000;
        perWeight[10] = 1000000;
        perWeight[11] = 1000000;
        perWeight[12] = 1000;
        perWeight[13] = 1000;
        perWeight[14] = 1000;
        perWeight[15] = 1;
        perWeight[16] = 1000000;
    }

    public void setup(Planet p) {
        float M,
                Mod;

        Position.copy(p.Position);
        Position.z -= p.Size + 10000;

        for (int i = 0; i != GameCon.NUM_PRODUCTS; i++) {
            numAvailable[i] = (int) (Math.random() * 100);

            price[i] = (int) (avPrice[i] * (1 + ((Math.random() - .5) / 10)));

            M = 0;

            if ((priceModifiers[i] & (AGRI + INDU)) != 0) {
                Mod = p.iIndustry - 3;
                Mod *= (float) (Math.random() * 10);

                if ((priceModifiers[i] & AGRI) != 0)
                    M += Mod;
                else
                    M -= Mod;
            }

            if ((priceModifiers[i] & (GOOD + BAD)) != 0) {
                Mod = (p.iPolitics - 4) * 0.7f;
                Mod *= (float) (Math.random() * 10);

                if ((priceModifiers[i] & GOOD) != 0)
                    M += Mod;
                else
                    M -= Mod;
            }

            if ((priceModifiers[i] & (LOTECH + HITECH)) != 0) {
                Mod = (p.iTechLevel - 5) * 0.6f;
                Mod *= (float) (Math.random() * 10);

                if ((priceModifiers[i] & LOTECH) != 0)
                    M += Mod;
                else
                    M -= Mod;
            }

            price[i] += (int) ((price[i] * M) / 100);
        }
    }

    public void run() {
        Matrix43 m = new Matrix43();

        Vector3D thrust = new Vector3D();

        thrust.set(0f, 0f, 1f);
        thrust.mul(Mat);
        m.rotateAbout(thrust, (float) (-Math.PI / 270));
        Mat.mul(m);
    }
}