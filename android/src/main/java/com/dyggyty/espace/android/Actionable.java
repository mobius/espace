package com.dyggyty.espace.android;

import com.dyggyty.espace.KeyHandler;

/**
 * @author vitaly.rudenya
 */
public interface Actionable {
    public KeyHandler.Action[] getActions();
}
