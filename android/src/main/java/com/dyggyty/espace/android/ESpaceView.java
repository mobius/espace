package com.dyggyty.espace.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.view.View;
import com.dyggyty.espace.KeyHandler;
import com.dyggyty.espace.game.GameCon;
import com.dyggyty.espace.game.model.ShipSim;
import com.dyggyty.espace.game.model.Universe;
import com.dyggyty.espace.graph.engine.Render;

/**
 * User: mobius
 * Date: 02.06.13
 * Time: 9:30
 */
public class ESpaceView extends View implements Actionable {

    private Render render = Render.getInstance(); // This is going to do all the drawing

    // Globals
    private int demoModel = 0;
    private static final int demoMods[] = {8, 17, 23, 28, 21, 16, 14, 15, 9, 2, 0, 1, -1};

    private Universe uni = new Universe();
    private KeyHandler keys = KeyHandler.getInstance();

    // Constructor
    public ESpaceView(Context context) {
        super(context);

        uni.initStaticLists();
        uni.loadModels();

        GameCon.CURRENT_MODE = GameCon.MODE_TITLE;
        GameCon.OLD_MODE = GameCon.MODE_START;
        GameCon.TIME_IN_MODE = 0;

        setOnTouchListener(ESpaceTouchListener.getInstance());

        render.initialise(uni);
    }

    private void initTitle() {
        uni.clearLists(true);

        uni.Cam.Mat.unit();
        uni.Cam.Mat.trans(0f, 0f, 0f);
        uni.Cam.Position.set(0f, 0f, 0f);

        ShipSim s = uni.getShip();

        if (s != null) {
            s.defaults(GameCon.CobraIII);
            demoModel = 0;
            s.PlayerCraft = false;
            s.pos(0, 0, -2800);
            s.mod(uni.Model[demoMods[demoModel]]);
            s.linkTo(uni.ShipUsed);
            s.ang(0, 0, 0);
            s.Rcur.set(0f, 0f, 0f);
            s.Rtar.set((float) (Math.PI / 70), (float) (Math.PI / 120), (float) (Math.PI / 82));
        }

        render.setupList(null);
    }

    private void initGame() {
        uni.clearLists(true);
        uni.initialiseGalaxy();

        // Setup player ships
        for (int i = 0; i != GameCon.NUM_PLAYERS; i++) {
            ShipSim s = uni.getShip();
            if (s != null) {
                uni.ShipPlayer[i] = s;
                s.linkTo(uni.ShipUsed);

                s.currentPlanet = uni.planets[s.iCurrentPlanet];
                s.selectedPlanet = uni.planets[s.iSelectedPlanet];
                s.PlayerCraft = true;
                s.PlayerId = i;

                // Zero rotations
                s.Rtar.zero();
                s.Rcur.zero();
                s.Mat.unit();
            } else {
                System.out.println("Couldn't allocate player ship");
                System.exit(1);
            }
        }

        render.setupList(uni.ShipPlayer[0]);
    }

    // Called back to draw the view. Also called by invalidate().
    @Override
    protected void onDraw(Canvas canvas) {
        // Update the position of the ball, including collision detection and reaction.
        update(canvas);

        invalidate();  // Force a re-draw
    }

    private void update(Canvas canvas) {
        if (GameCon.CURRENT_MODE != GameCon.OLD_MODE) {
            GameCon.OLD_MODE = GameCon.CURRENT_MODE;
            GameCon.TIME_IN_MODE = 0;
        } else {
            GameCon.TIME_IN_MODE++;
        }

        switch (GameCon.CURRENT_MODE) {
            case GameCon.MODE_TITLE:
                if (GameCon.TIME_IN_MODE == 0) {
                    initTitle();
                } else {
                    uni.Cam.Position.z += 100;
                    ShipSim s = (ShipSim) uni.ShipUsed.Next;
                    if (s != null) {
                        int t = GameCon.TIME_IN_MODE % 700;
                        float off;

                        s.run(uni);

                        if (t == 0)
                            s.randomColour();

                        if (t < 100)
                            off = (100 - t) * 150;
                        else if (t > 600)
                            off = (t - 600) * 150;
                        else
                            off = 0;

                        if (t == 0) {
                            demoModel = demoModel + 1;
                            if (demoMods[demoModel] == -1)
                                demoModel = 0;
                            s.mod(uni.Model[demoMods[demoModel]]);
                        }

                        s.Position.copy(uni.Cam.Position);
                        s.Position.z = uni.Cam.Position.z + 500 + off * 2;
                    }

                    KeyHandler.Action action = keys.getCompletedAction();
                    if (action != null && KeyHandler.Action.TOUCH == action) {
                        Intent intent = new Intent(getContext(), DockActivity.class);
                        getContext().startActivity(intent);
                        GameCon.CURRENT_MODE = GameCon.MODE_START;
                    }
                }
                break;

            case GameCon.MODE_START:
                initGame();
                GameCon.CURRENT_MODE = GameCon.MODE_DOCKED;
                break;
        }

        render.rePaint(canvas);
    }

    // Called back when the view is first created or its size changes.
    @Override
    public void onSizeChanged(int w, int h, int oldW, int oldH) {
        render.setScreenWidth(w);
        render.setScreenHeight(h);
    }

    @Override
    public KeyHandler.Action[] getActions() {
        return new KeyHandler.Action[]{KeyHandler.Action.TOUCH};
    }
}
