package com.dyggyty.espace.android;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;
import com.dyggyty.espace.KeyHandler;
import com.dyggyty.espace.game.GameCon;
import com.dyggyty.espace.game.model.Planet;
import com.dyggyty.espace.game.model.ShipSim;
import com.dyggyty.espace.game.model.Station;
import com.dyggyty.espace.game.model.Sun3d;
import com.dyggyty.espace.game.model.Universe;
import com.dyggyty.espace.graph.engine.Render;
import com.dyggyty.espace.graph.engine.Vector3D;

/**
 * User: mobius
 * Date: 20.07.13
 * Time: 11:58
 */
public class FlightView extends View {

    private KeyHandler keyHandler = KeyHandler.getInstance();
    private Render render = Render.getInstance();

    public FlightView(Context context) {
        super(context);
        setOnTouchListener(ESpaceTouchListener.getInstance());
    }

    // Called back to draw the view. Also called by invalidate().
    @Override
    protected void onDraw(Canvas canvas) {
        // Update the position of the ball, including collision detection and reaction.
        update(canvas);
        invalidate();  // Force a re-draw
    }

    private void update(Canvas canvas) {
        if (GameCon.CURRENT_MODE != GameCon.OLD_MODE) {
            GameCon.OLD_MODE = GameCon.CURRENT_MODE;
            GameCon.TIME_IN_MODE = 0;
        } else {
            GameCon.TIME_IN_MODE++;
        }

        Universe universe = render.getUniverse();
        switch (GameCon.CURRENT_MODE) {
            case GameCon.MODE_LAUNCH:
                if ((GameCon.TIME_IN_MODE / 50) > 0) {
                    initSpace(0);
                    GameCon.CURRENT_MODE = GameCon.MODE_SPACE;
                }
                break;

            case GameCon.MODE_SPACE:
                keyHandler.processKeys(universe.ShipPlayer[0]);

                // Run ship handlers
                ShipSim s = (ShipSim) universe.ShipUsed.Next;
                while (s != null) {
                    ShipSim sNext = s.Next();
                    s.run(universe);
                    s = sNext;
                }


/*
                if (keys.iDock != 0) {
                    keys.iDock = 0;

                    if (uni.ShipPlayer[0].autoEngaged()) {
                        uni.ShipPlayer[0].autoStop();
                    } else {
                        if (uni.ShipPlayer[0].Upgrade[6] && uni.ShipPlayer[0].bStation) {
                            uni.ShipPlayer[0].followStation(uni.station);
                        }
                    }
                }

                if (keys.iJump == 1 && !uni.ShipPlayer[0].bStation) {
                    uni.ShipPlayer[0].jump((Obj3d) uni.ShipUsed.Next);
                }
*/

                universe.event();

                universe.Cam.update();
                break;

            case GameCon.MODE_HYPER:
                if ((GameCon.TIME_IN_MODE / 50) > 0) {
                    GameCon.CURRENT_MODE = GameCon.MODE_SPACE;

                    // Set new system
                    universe.ShipPlayer[0].iCurrentPlanet = universe.ShipPlayer[0].iSelectedPlanet;
                    initSpace(1);
                }
                break;

            case GameCon.MODE_DOCK:
                if (GameCon.TIME_IN_MODE > 100) {
                    universe.ShipPlayer[0].autoStop();
                    universe.ShipPlayer[0].MissileState = 0;
                    GameCon.CURRENT_MODE = GameCon.MODE_DOCKED;
                }
                break;

            case GameCon.MODE_AUTODOCK1:
                break;

            case GameCon.MODE_AUTODOCK2:
                GameCon.CURRENT_MODE = GameCon.MODE_TITLE;
                GameCon.TIME_IN_MODE = 0;
                break;
        }

        render.rePaint(canvas);
    }

    private void initSpace(int type) {

        Universe universe = render.getUniverse();

        ShipSim s = universe.ShipPlayer[0];
        Vector3D off;

        universe.clearLists(false);

        s.currentPlanet = universe.planets[s.iCurrentPlanet];

        Planet planet = s.currentPlanet;
        Sun3d sun = universe.sun;
        Station station = universe.station;

        station.setup(planet);
        station.mod(universe.Model[11]);
        sun.setup(s.iCurrentPlanet);

        if (type == 0) {
            off = new Vector3D(universe.station.Position);
            off.z += 4000;
        } else {
            off = new Vector3D(0, 0, 0);
        }

        s.mod(universe.Model[0]);
        s.Position.copy(off);
        s.Mat.unit();
        s.fSpeedTar = 10;
        s.fSpeedCur = s.fSpeedMax;
        s.Position.z -= 3000;
        s.PlayerCraft = true;
        s.PlayerId = 0;
        s.MissileState = 0;
        s.iView = 1;
        keyHandler.iView = 1;

        planet.linkTo(universe.Bodies);
        station.linkTo(universe.Bodies);
        sun.linkTo(universe.Bodies);

        universe.Cam.update(s);
        render.setupList(s);
    }
}
