package com.dyggyty.espace.android;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * User: mobius
 * Date: 20.07.13
 * Time: 14:49
 */
public class Const {
    private static final AtomicInteger IND = new AtomicInteger(48720839);

    public static final int FLIGHT_ACTIVITY = IND.getAndIncrement();
}
