package com.dyggyty.espace.android.ship;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.dyggyty.espace.android.FlightActivity;
import com.dyggyty.espace.android.R;
import com.dyggyty.espace.game.GameCon;
import com.dyggyty.espace.game.model.ShipSim;
import com.dyggyty.espace.graph.engine.Render;

/**
 * @author vitaly.rudenya
 */
public class StatusFragment extends Fragment {

    private ShipSim player = Render.getInstance().getPlayer();

    /**
     * The Fragment's UI is just a simple text view showing its
     * instance number.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.ship_status, container, false);

        TextView criminalStatus = (TextView) v.findViewById(R.id.player_criminal_status);
        criminalStatus.setText(GameCon.POLICE[player.PoliceStatus]);

        TextView credits = (TextView) v.findViewById(R.id.player_credits);
        credits.setText("" + ((float) player.Credits) / 10);

        TextView fuel = (TextView) v.findViewById(R.id.player_fuel);
        fuel.setText("" + player.Fuel);

        String noneFitted = container.getContext().getResources().getString(R.string.none_fitted);

        TextView weaponryFront = (TextView) v.findViewById(R.id.weaponry_front);
        if (player.LaserType[0] == -1) {
            weaponryFront.setText(noneFitted);
        } else {
            weaponryFront.setText(GameCon.UPGRADES[player.LaserType[0]]);
        }

        TextView weaponryRear = (TextView) v.findViewById(R.id.weaponry_rear);
        if (player.LaserType[1] == -1) {
            weaponryRear.setText(noneFitted);
        } else {
            weaponryRear.setText(GameCon.UPGRADES[player.LaserType[1]]);
        }

        TextView weaponryLeft = (TextView) v.findViewById(R.id.weaponry_left);
        if (player.LaserType[2] == -1) {
            weaponryLeft.setText(noneFitted);
        } else {
            weaponryLeft.setText(GameCon.UPGRADES[player.LaserType[2]]);
        }

        TextView weaponryRight = (TextView) v.findViewById(R.id.weaponry_right);
        if (player.LaserType[3] == -1) {
            weaponryRight.setText(noneFitted);
        } else {
            weaponryRight.setText(GameCon.UPGRADES[player.LaserType[3]]);
        }

        StringBuffer extras = new StringBuffer();
        byte extrasSize = 0;
        for (int i = 0; i != 8; i++) {
            if (player.Upgrade[i]) {
                if (extrasSize++ != 0) {
                    extras.append("\n");
                }
                extras.append(GameCon.UPGRADES[6 + i]);
            }
        }

        TextView extrasText = (TextView) v.findViewById(R.id.extras);
        if (extrasSize == 0) {
            extrasText.setLines(1);
            extrasText.setText(noneFitted);
        } else {
            extrasText.setLines(extrasSize);
            extrasText.setText(extras);
        }

        return v;
    }
}
