package com.dyggyty.espace.android;

import android.view.MotionEvent;
import android.view.View;
import com.dyggyty.espace.KeyHandler;

/**
 * @author vitaly.rudenya
 */
public class ESpaceTouchListener implements View.OnTouchListener {

    private static final ESpaceTouchListener onTouchListener = new ESpaceTouchListener();
    private KeyHandler keyHandler = KeyHandler.getInstance();

    private ESpaceTouchListener() {
    }

    public static ESpaceTouchListener getInstance() {
        return onTouchListener;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (view instanceof Actionable) {
            if (MotionEvent.ACTION_DOWN == motionEvent.getAction() ||
                    MotionEvent.ACTION_POINTER_DOWN == motionEvent.getAction()) {
                keyHandler.processKeyPress((int) motionEvent.getX(), (int) motionEvent.getY(),
                        ((Actionable) view).getActions());
            } else {
                keyHandler.processKeyUp();
            }
        }
        return true;
    }
}
